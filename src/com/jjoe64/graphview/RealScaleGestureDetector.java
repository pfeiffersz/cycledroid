package com.jjoe64.graphview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.ScaleGestureDetector;

/**
 * Copyright (C) 2011 Jonas Gehring Licensed under the GNU Lesser General Public License (LGPL)
 * http://www.gnu.org/licenses/lgpl.html
 */
@SuppressLint("NewApi")
public class RealScaleGestureDetector extends ScaleGestureDetector {
	public RealScaleGestureDetector(Context context,
			final com.jjoe64.graphview.ScaleGestureDetector fakeScaleGestureDetector,
			final com.jjoe64.graphview.ScaleGestureDetector.SimpleOnScaleGestureListener fakeListener) {
		super(context, new android.view.ScaleGestureDetector.SimpleOnScaleGestureListener() {
			@Override
			public boolean onScale(ScaleGestureDetector detector) {
				return fakeListener.onScale(fakeScaleGestureDetector);
			}
		});
	}
}
