package com.jjoe64.graphview;

public class GraphViewData {
	public final float valueX;
	public final float valueY;
	
	public GraphViewData(float valueX, float valueY) {
		this.valueX = valueX;
		this.valueY = valueY;
	}
}
