/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class AppInfo {
	private static final String PREFERENCES_NAME = "app_info";
	
	private final SharedPreferences preferences;
	
	private static enum PreferenceKey {
		VERSION, MOVED_SCREEN, LAST_INTERVAL, FOLLOW, MAP_ROTATE, POSITION_BEARING, POSITION_TILT, POSITION_ZOOM,
		POSITION_LATITUDE, POSITION_LONGITUDE, MAP_TYPE, LAST_EXPORT_PATH, LAST_IMPORT_PATH, LAST_SAVE_GRAPH_PATH,
		ENTERED_SUMMARY, DONATED,
	}
	
	// default values
	public static final int NO_VERSION = -1;
	public static final int NO_INTERVAL = 10;
	public static final int NO_MAP_TYPE = GoogleMap.MAP_TYPE_NORMAL;
	public static final String NO_EXPORT_PATH = null;
	public static final String NO_IMPORT_PATH = null;
	public static final String NO_SAVE_GRAPH_PATH = null;
	
	private void setBoolean(PreferenceKey key, boolean value) {
		Editor editor = preferences.edit();
		editor.putBoolean(key.name(), value);
		editor.commit();
	}
	
	private void setInt(PreferenceKey key, int value) {
		Editor editor = preferences.edit();
		editor.putInt(key.name(), value);
		editor.commit();
	}
	
	private void setString(PreferenceKey key, String value) {
		Editor editor = preferences.edit();
		editor.putString(key.name(), value);
		editor.commit();
	}
	
	public AppInfo(Context context) {
		preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
	}
	
	public int getCurrentVersion() {
		return preferences.getInt(PreferenceKey.VERSION.name(), NO_VERSION);
	}
	
	public void setCurrentVersion(int newVersion) {
		setInt(PreferenceKey.VERSION, newVersion);
	}
	
	public boolean getMovedScreen() {
		return preferences.getBoolean(PreferenceKey.MOVED_SCREEN.name(), false);
	}
	
	public void setMovedScreen(boolean movedScreen) {
		setBoolean(PreferenceKey.MOVED_SCREEN, movedScreen);
	}
	
	public int getLastInterval() {
		return preferences.getInt(PreferenceKey.LAST_INTERVAL.name(), NO_INTERVAL);
	}
	
	public void setLastInterval(int lastInterval) {
		setInt(PreferenceKey.LAST_INTERVAL, lastInterval);
	}
	
	public boolean getFollow() {
		return preferences.getBoolean(PreferenceKey.FOLLOW.name(), true);
	}
	
	public void setFollow(boolean follow) {
		setBoolean(PreferenceKey.FOLLOW, follow);
	}
	
	public boolean getMapRotate() {
		return preferences.getBoolean(PreferenceKey.MAP_ROTATE.name(), true);
	}
	
	public void setMapRotate(boolean mapRotate) {
		setBoolean(PreferenceKey.MAP_ROTATE, mapRotate);
	}
	
	public CameraPosition getLastCameraPosition() {
		CameraPosition.Builder builder = CameraPosition.builder();
		builder.bearing(preferences.getFloat(PreferenceKey.POSITION_BEARING.name(), 0.0f));
		builder.tilt(preferences.getFloat(PreferenceKey.POSITION_TILT.name(), 0.0f));
		builder.zoom(preferences.getFloat(PreferenceKey.POSITION_ZOOM.name(), 0.0f));
		float latitude = preferences.getFloat(PreferenceKey.POSITION_LATITUDE.name(), 0.0f);
		float longitude = preferences.getFloat(PreferenceKey.POSITION_LONGITUDE.name(), 0.0f);
		LatLng target = new LatLng(latitude, longitude);
		builder.target(target);
		return builder.build();
	}
	
	public void setLastCameraPosition(CameraPosition cameraPosition) {
		Editor editor = preferences.edit();
		editor.putFloat(PreferenceKey.POSITION_BEARING.name(), cameraPosition.bearing);
		editor.putFloat(PreferenceKey.POSITION_TILT.name(), cameraPosition.tilt);
		editor.putFloat(PreferenceKey.POSITION_ZOOM.name(), cameraPosition.zoom);
		editor.putFloat(PreferenceKey.POSITION_LATITUDE.name(), (float)cameraPosition.target.latitude);
		editor.putFloat(PreferenceKey.POSITION_LONGITUDE.name(), (float)cameraPosition.target.longitude);
		editor.commit();
	}
	
	public int getLastMapType() {
		return preferences.getInt(PreferenceKey.MAP_TYPE.name(), NO_MAP_TYPE);
	}
	
	public void setLastMapType(int mapType) {
		setInt(PreferenceKey.MAP_TYPE, mapType);
	}
	
	public String getLastExportPath() {
		return preferences.getString(PreferenceKey.LAST_EXPORT_PATH.name(), NO_EXPORT_PATH);
	}
	
	public void setLastExportPath(String exportPath) {
		setString(PreferenceKey.LAST_EXPORT_PATH, exportPath);
	}
	
	public String getLastImportPath() {
		return preferences.getString(PreferenceKey.LAST_IMPORT_PATH.name(), NO_IMPORT_PATH);
	}
	
	public void setLastImportPath(String importPath) {
		setString(PreferenceKey.LAST_IMPORT_PATH, importPath);
	}
	
	public String getLastSaveGraphPath() {
		return preferences.getString(PreferenceKey.LAST_SAVE_GRAPH_PATH.name(), NO_SAVE_GRAPH_PATH);
	}
	
	public void setLastSaveGraphPath(String saveGraphPath) {
		setString(PreferenceKey.LAST_SAVE_GRAPH_PATH, saveGraphPath);
	}
	
	public boolean getEnteredSummary() {
		return preferences.getBoolean(PreferenceKey.ENTERED_SUMMARY.name(), false);
	}
	
	public void setEnteredSummary(boolean enteredSummary) {
		setBoolean(PreferenceKey.ENTERED_SUMMARY, enteredSummary);
	}
	
	public boolean getDonated() {
		return preferences.getBoolean(PreferenceKey.DONATED.name(), false);
	}
	
	public void setDonated(boolean donated) {
		setBoolean(PreferenceKey.DONATED, donated);
	}
}
