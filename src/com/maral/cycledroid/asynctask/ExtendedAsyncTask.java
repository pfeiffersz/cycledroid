/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.asynctask;

import android.os.AsyncTask;

public abstract class ExtendedAsyncTask extends AsyncTask<Void, Integer, Void> {
	public static final long NO_ID = -1;
	
	private static long nextId = 0;
	
	private final long id;
	private final AsyncTaskReceiver receiver;
	private int progress = 0;
	
	public ExtendedAsyncTask(AsyncTaskReceiver receiver) {
		this.receiver = receiver;
		id = nextId++;
	}
	
	protected abstract void executeTask();
	
	@Override
	protected void onPreExecute() {
		receiver.taskStarts(this);
		receiver.updateProgress(this, progress);
	}
	
	@Override
	protected void onPostExecute(Void result) {
		receiver.taskFinishes(this);
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		executeTask();
		return null;
	}
	
	@Override
	protected void onProgressUpdate(Integer... progress) {
		this.progress = progress[0];
		receiver.updateProgress(this, this.progress);
	}
	
	public void updateProgress(int newProgress) {
		publishProgress(newProgress);
	}
	
	public int getProgress() {
		return progress;
	}
	
	public long getId() {
		return id;
	}
}
