/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.widget.pager;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

public class CyclicPagerAdapter extends PagerAdapter {
	public static int FAKED_COUNT = 50000;
	
	private final PagerAdapter adapter;
	
	public CyclicPagerAdapter(PagerAdapter adapter) {
		this.adapter = adapter;
	}
	
	@Override
	public int getCount() {
		return FAKED_COUNT;
	}
	
	public int getRealCount() {
		return adapter.getCount();
	}
	
	@Override
	public boolean isViewFromObject(View view, Object object) {
		return adapter.isViewFromObject(view, object);
	}
	
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		return adapter.instantiateItem(container, realPosition(position));
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		adapter.destroyItem(container, realPosition(position), object);
	}
	
	private int realPosition(int position) {
		return position % adapter.getCount();
	}
	
	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		adapter.notifyDataSetChanged();
	}
}
