/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.Button;

public class ValidatablePreference extends EditTextPreference {
	public static interface ValueValidator {
		public boolean validate(Editable s);
	}
	
	private class Controller implements TextWatcher {
		public void afterTextChanged(Editable s) {
			try {
				Button button = ((AlertDialog)getDialog()).getButton(AlertDialog.BUTTON_POSITIVE);
				button.setEnabled(validator == null || validator.validate(s));
			} catch(NullPointerException exception) {} // there may be no dialog etc.
		}
		
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
		
		public void onTextChanged(CharSequence s, int start, int before, int count) {}
	}
	
	private final Controller controller = new Controller();
	
	private ValueValidator validator = null;
	
	public ValidatablePreference(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	public ValidatablePreference(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public void setValidator(ValueValidator validator) {
		this.validator = validator;
	}
	
	@Override
	protected void showDialog(Bundle state) {
		super.showDialog(state);
		getEditText().addTextChangedListener(controller);
		controller.afterTextChanged(getEditText().getText());
	}
}
