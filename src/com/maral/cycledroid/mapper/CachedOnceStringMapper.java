/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.mapper;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;

public class CachedOnceStringMapper<T> implements StringMapper<T> {
	private final Map<T, String> cache = new HashMap<T, String>();
	private final Map<String, T> reverse = new HashMap<String, T>();
	
	public CachedOnceStringMapper(ResourceMapper<T> resources, Context context, T[] objects) {
		SimpleStringMapper<T> subMapper = new SimpleStringMapper<T>(resources, context);
		for(T object : objects) {
			String string = subMapper.getString(object);
			cache.put(object, string);
			reverse.put(string, object);
		}
	}
	
	public String getString(T object) {
		return cache.get(object);
	}
	
	public T reverseMapping(String string) {
		return reverse.get(string);
	}
}
