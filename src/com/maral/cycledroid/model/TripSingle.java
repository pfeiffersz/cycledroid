/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.model;

import java.util.Arrays;
import java.util.Iterator;
import java.util.ListIterator;

import com.maral.cycledroid.CircularBuffer;

import android.location.Location;

public class TripSingle extends Trip {
	private static final float MIN_ALTITUDE_CHANGE = 5.0f;
	private static final float MAX_ALTITUDE_CHANGE = 10.0f;
	
	private static final int SLOPE_SAMPLES = 3;
	private static final float GRAVITY_ACCEL = 9.80665f;
	private static final float DRAG_FACTOR = 0.13154151999360186f;
	
	private Long id; // from database, null if not yet in database
	private String name;
	private String description;
	private boolean paused = true;
	private float distance = 0.0f; // [meter]
	private float time = 0.0f; // [millisecond];
	private Float maxSpeed = null; // [meter/second]
	private float elevationAsc = 0.0f; // [meter]
	private float elevationDesc = 0.0f; // [meter]
	private Float minAltitude = null; // [meter]
	private Float maxAltitude = null; // [meter]
	private float totalTime = 0.0f; // [millisecond]
	private Float slope = null; // [degree]
	private Float powerFactor = null;
	private Float initialAltitude = null; // [meter]
	private Float finalAltitude = null; // [meter]
	private Long startTime = null; // POSIX time [millisecond]
	private Long endTime = null; // POSIX time [millisecond]
	
	// for calculating elevation
	private Float savedAltitude = null;
	
	private final CircularBuffer<Location> lastPoints = new CircularBuffer<Location>(10);
	
	public TripSingle(Long id, String name, String description, Float maxSpeed, float distance, float time,
			float elevationAsc, float elevationDesc, Float minAltitude, Float maxAltitude, float totalTime,
			Float initialAltitude, Float finalAltitude, Long startTime, Long endTime) {
		this(id, name, description);
		this.maxSpeed = maxSpeed;
		this.distance = distance;
		this.time = time;
		this.elevationAsc = elevationAsc;
		this.elevationDesc = elevationDesc;
		this.minAltitude = minAltitude;
		this.maxAltitude = maxAltitude;
		this.totalTime = totalTime;
		this.initialAltitude = initialAltitude;
		this.finalAltitude = finalAltitude;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
	public TripSingle(Long id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public TripSingle(String name, String description) {
		this(null, name, description);
	}
	
	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public String getDescription() {
		return description;
	}
	
	@Override
	public boolean providesEdit() {
		return true;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public void edit(String name, String description) {
		this.name = name;
		this.description = description;
		setChanged();
		notifyObservers();
	}
	
	@Override
	public boolean providesTracking() {
		return true;
	}
	
	private Location getLastPoint() {
		return lastPoints.size() == 0? null : lastPoints.iterator().next();
	}
	
	@Override
	public void addPoint(Location point) { // altitude and speed are obligatory, bearing not
		paused = false;
		Location lastPoint = getLastPoint();
		if(lastPoint != null && point.getTime() - lastPoint.getTime() > PART_TIME)
			lastPoint = null;
		float currentAltitude = (float)point.getAltitude();
		
		if(lastPoint == null)
			savedAltitude = currentAltitude;
		else {
			// calculate distance and time
			distance += lastPoint.distanceTo(point);
			long deltaTime = point.getTime() - lastPoint.getTime();
			time += deltaTime;
			totalTime += deltaTime;
		}
		
		// calculate elevation
		float change = currentAltitude - savedAltitude;
		if(Math.abs(change) >= MIN_ALTITUDE_CHANGE) {
			if(Math.abs(change) <= MAX_ALTITUDE_CHANGE) {
				if(change < 0)
					elevationDesc += -change;
				else
					elevationAsc += change;
			}
			savedAltitude = currentAltitude;
		}
		
		// calculate other parameters
		maxSpeed = Math.max(maxSpeed == null? Float.NEGATIVE_INFINITY : maxSpeed, point.getSpeed());
		maxAltitude = Math.max(maxAltitude == null? Float.NEGATIVE_INFINITY : maxAltitude, currentAltitude);
		minAltitude = Math.min(minAltitude == null? Float.POSITIVE_INFINITY : minAltitude, currentAltitude);
		if(initialAltitude == null)
			initialAltitude = currentAltitude;
		finalAltitude = currentAltitude;
		if(startTime == null)
			startTime = point.getTime();
		endTime = point.getTime();
		lastPoints.add(point);
		updateSlope();
		//updatePowerFactor();
		
		performNotifyObservers(point);
	}
	
	private void performNotifyObservers(Object data) {
		setChanged();
		notifyObservers(data);
	}
	
	@Override
	public void increaseTotalTime(float deltaTime) {
		totalTime += deltaTime;
		performNotifyObservers(null);
	}
	
	private void pauseNoNotify() {
		paused = true;
		powerFactor = null;
	}
	
	@Override
	public void pause() {
		pauseNoNotify();
		performNotifyObservers(null);
	}
	
	@Override
	public boolean isPaused() {
		return paused;
	}
	
	@Override
	public void stop() {
		pauseNoNotify();
		lastPoints.clear();
		slope = null;
		performNotifyObservers(null);
	}
	
	@Override
	public float getDistance() {
		return distance;
	}
	
	@Override
	public float getTime() {
		return time;
	}
	
	@Override
	public Float getCurrentSpeed() {
		Location lastPoint = getLastPoint();
		return paused || lastPoint == null? 0.0f : lastPoint.getSpeed();
	}
	
	@Override
	public Float getMaxSpeed() {
		return maxSpeed;
	}
	
	@Override
	public Float getAltitude() {
		Location lastPoint = getLastPoint();
		return lastPoint == null? null : (float)lastPoint.getAltitude();
	}
	
	@Override
	public float getElevationAsc() {
		return elevationAsc;
	}
	
	@Override
	public float getElevationDesc() {
		return elevationDesc;
	}
	
	@Override
	public Float getMinAltitude() {
		return minAltitude;
	}
	
	@Override
	public Float getMaxAltitude() {
		return maxAltitude;
	}
	
	@Override
	public float getTotalTime() {
		return totalTime;
	}
	
	@Override
	public Float getBearing() {
		Location lastPoint = getLastPoint();
		if(lastPoint == null)
			return null;
		float bearing = lastPoint.getBearing();
		return bearing == 0.0f? null : bearing; // 0.0f means no bearing
	}
	
	@Override
	public Float getSlope() {
		return slope;
	}
	
	@Override
	public Float getPowerFactor() {
		return powerFactor;
	}
	
	@Override
	public Float getInitialAltitude() {
		return initialAltitude;
	}
	
	@Override
	public Float getFinalAltitude() {
		return finalAltitude;
	}
	
	@Override
	public Long getStartTime() {
		return startTime;
	}
	
	@Override
	public Long getEndTime() {
		return endTime;
	}
	
	@Override
	public boolean providesGraphs() {
		return true;
	}
	
	@Override
	public boolean providesMap() {
		return true;
	}
	
	@Override
	public boolean providesExport() {
		return true;
	}
	
	@Override
	public boolean providesShare() {
		return true;
	}
	
	private static float median(float[] values) {
		Arrays.sort(values);
		if(values.length % 2 == 1)
			return values[values.length / 2];
		return (values[values.length / 2] + values[values.length / 2 - 1]) / 2.0f;
	}
	
	private static float slopeTangens(Location a, Location b) {
		return ((float)(a.getAltitude() - b.getAltitude())) / a.distanceTo(b);
	}
	
	private void updateSlope() {
		if(lastPoints.size() < 2)
			slope = null;
		else if(lastPoints.size() < 2 * SLOPE_SAMPLES) {
			Location a = lastPoints.getFirst();
			Location b = lastPoints.getLast();
			slope = (float)Math.toDegrees(Math.atan(slopeTangens(a, b)));
		} else {
			float[] slopes = new float[SLOPE_SAMPLES];
			Iterator<Location> start = lastPoints.iterator();
			ListIterator<Location> end = lastPoints.listIterator(lastPoints.size() - 1 - SLOPE_SAMPLES);
			for(int i = 0; i < SLOPE_SAMPLES; ++i) {
				Location a = start.next();
				Location b = end.next();
				slopes[i] = slopeTangens(a, b);
			}
			slope = (float)Math.toDegrees(Math.atan(median(slopes)));
		}
	}
	
	@SuppressWarnings("unused")
	private void updatePowerFactor() { // maybe in some future version
		if(lastPoints.size() < 2)
			powerFactor = null;
		else {
			Iterator<Location> iterator = lastPoints.iterator();
			Location last = iterator.next();
			Location previous = iterator.next();
			float timeDiff = ((float)(last.getTime() - previous.getTime())) / 1000.0f;
			float locDiff = last.distanceTo(previous);
			float accel = (last.getSpeed() - previous.getSpeed()) / timeDiff * locDiff;
			float climb = (float)(last.getAltitude() - previous.getAltitude()) * GRAVITY_ACCEL;
			float drag = last.getSpeed() * last.getSpeed() * DRAG_FACTOR * locDiff;
			powerFactor = (accel + climb + drag) / timeDiff;
		}
	}
}
