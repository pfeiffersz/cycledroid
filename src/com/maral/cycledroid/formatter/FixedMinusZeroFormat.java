/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.formatter;

import java.text.DecimalFormat;
import java.text.FieldPosition;

@SuppressWarnings("serial")
class FixedMinusZeroFormat extends DecimalFormat { // solves problem with "-0" (or "-0.00" etc.)
	public FixedMinusZeroFormat(String pattern) {
		super(pattern);
	}
	
	@Override
	public StringBuffer format(double value, StringBuffer buffer, FieldPosition field) {
		String formatted = super.format(value, new StringBuffer(), field).toString();
		return buffer.append(formatted.replaceAll("^-(?=0(.0*)?$)", "").replaceAll("^-(?=0(,0*)?$)", ""));
		// http://stackoverflow.com/questions/11929096/negative-sign-in-case-of-zero-in-java
	}
}
