/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.formatter;

import java.util.Observable;

import android.content.Context;

import com.maral.cycledroid.R;
import com.maral.cycledroid.formatter.UnitResourceMapper.AltitudeMapper;
import com.maral.cycledroid.formatter.UnitResourceMapper.AngleMapper;
import com.maral.cycledroid.formatter.UnitResourceMapper.DistanceMapper;
import com.maral.cycledroid.formatter.UnitResourceMapper.EnergyMapper;
import com.maral.cycledroid.formatter.UnitResourceMapper.PaceMapper;
import com.maral.cycledroid.formatter.UnitResourceMapper.PowerMapper;
import com.maral.cycledroid.formatter.UnitResourceMapper.SpeedMapper;
import com.maral.cycledroid.formatter.UnitResourceMapper.VolumeMapper;
import com.maral.cycledroid.formatter.UnitResourceMapper.WeightMapper;
import com.maral.cycledroid.mapper.SimpleStringMapper;
import com.maral.cycledroid.mapper.StringMapper;
import com.maral.cycledroid.model.HealthCalculator;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.model.Unit;
import com.maral.cycledroid.model.Unit.Altitude;
import com.maral.cycledroid.model.Unit.Angle;
import com.maral.cycledroid.model.Unit.Distance;
import com.maral.cycledroid.model.Unit.Duration;
import com.maral.cycledroid.model.Unit.Energy;
import com.maral.cycledroid.model.Unit.Pace;
import com.maral.cycledroid.model.Unit.Power;
import com.maral.cycledroid.model.Unit.Speed;
import com.maral.cycledroid.model.Unit.Volume;
import com.maral.cycledroid.model.Unit.Weight;

public class PairFormatter extends Observable {
	private final ValueFormatter formatter = new ValueFormatter();
	
	private final StringMapper<Altitude> altitudeMapper;
	private final StringMapper<Angle> angleMapper;
	private final StringMapper<Distance> distanceMapper;
	private final StringMapper<Energy> energyMapper;
	private final StringMapper<Pace> paceMapper;
	private final StringMapper<Power> powerMapper;
	private final StringMapper<Speed> speedMapper;
	private final StringMapper<Volume> volumeMapper;
	private final StringMapper<Weight> weightMapper;
	
	private final Context context;
	
	private Altitude altitudeUnit = null;
	private Angle bearingUnit = null;
	private Distance distanceUnit = null;
	private Duration durationUnit = null;
	private Energy energyUnit = null;
	private Pace paceUnit = null;
	private Power powerUnit = null;
	private Angle slopeUnit = null;
	private Speed speedUnit = null;
	private Volume volumeUnit = null;
	private Weight weightUnit = null;
	
	public PairFormatter(Context context) {
		altitudeMapper = new SimpleStringMapper<Altitude>(AltitudeMapper.getInstance(), context);
		angleMapper = new SimpleStringMapper<Angle>(AngleMapper.getInstance(), context);
		distanceMapper = new SimpleStringMapper<Distance>(DistanceMapper.getInstance(), context);
		energyMapper = new SimpleStringMapper<Energy>(EnergyMapper.getInstance(), context);
		paceMapper = new SimpleStringMapper<Pace>(PaceMapper.getInstance(), context);
		powerMapper = new SimpleStringMapper<Power>(PowerMapper.getInstance(), context);
		speedMapper = new SimpleStringMapper<Speed>(SpeedMapper.getInstance(), context);
		volumeMapper = new SimpleStringMapper<Volume>(VolumeMapper.getInstance(), context);
		weightMapper = new SimpleStringMapper<Weight>(WeightMapper.getInstance(), context);
		this.context = context;
	}
	
	private String formatNoValue(String value) {
		if(value == null)
			return context.getString(R.string.no_value);
		return value;
	}
	
	private void performNotifyObservers() {
		setChanged();
		notifyObservers();
	}
	
	public void setAltitudeUnit(Altitude altitudeUnit) {
		this.altitudeUnit = altitudeUnit;
		performNotifyObservers();
	}
	
	public void setBearingUnit(Angle bearingUnit) {
		this.bearingUnit = bearingUnit;
		performNotifyObservers();
	}
	
	public void setDistanceUnit(Distance distanceUnit) {
		this.distanceUnit = distanceUnit;
		performNotifyObservers();
	}
	
	public void setDurationUnit(Duration durationUnit) {
		this.durationUnit = durationUnit;
		performNotifyObservers();
	}
	
	public void setEnergyUnit(Energy energyUnit) {
		this.energyUnit = energyUnit;
		performNotifyObservers();
	}
	
	public void setPaceUnit(Pace paceUnit) {
		this.paceUnit = paceUnit;
		performNotifyObservers();
	}
	
	public void setPowerUnit(Power powerUnit) {
		this.powerUnit = powerUnit;
		performNotifyObservers();
	}
	
	public void setSlopeUnit(Angle slopeUnit) {
		this.slopeUnit = slopeUnit;
		performNotifyObservers();
	}
	
	public void setSpeedUnit(Speed speedUnit) {
		this.speedUnit = speedUnit;
		performNotifyObservers();
	}
	
	public void setVolumeUnit(Volume volumeUnit) {
		this.volumeUnit = volumeUnit;
		performNotifyObservers();
	}
	
	public void setWeightUnit(Weight weightUnit) {
		this.weightUnit = weightUnit;
		performNotifyObservers();
	}
	
	private ValuePair formatAltitude(Float value) {
		Float converted = Unit.convertAltitude(value, Trip.UNIT_ALTITUDE, altitudeUnit);
		String first = formatNoValue(formatter.formatAltitude(converted, altitudeUnit));
		String second = altitudeMapper.getString(altitudeUnit);
		return new ValuePair(first, second);
	}
	
	private ValuePair formatSpeed(Float value) {
		Float converted = Unit.convertSpeed(value, Trip.UNIT_SPEED, speedUnit);
		String first = formatNoValue(formatter.formatSpeed(converted, speedUnit));
		String second = speedMapper.getString(speedUnit);
		return new ValuePair(first, second);
	}
	
	private ValuePair formatDuration(Float value) {
		Float converted = Unit.convertDuration(value, Trip.UNIT_DURATION, durationUnit);
		return new ValuePair(formatNoValue(formatter.formatDuration(converted, durationUnit)));
	}
	
	private ValuePair formatPace(Float value) {
		Float converted = Unit.convertPace(value, Trip.UNIT_PACE, paceUnit);
		String first = formatNoValue(formatter.formatPace(converted, paceUnit));
		String second = paceMapper.getString(paceUnit);
		return new ValuePair(first, second);
	}
	
	private ValuePair formatDate(Long value) {
		return new ValuePair(formatNoValue(formatter.formatDate(value)));
	}
	
	public ValuePair getCurrentSpeed(Trip trip) {
		return formatSpeed(trip.getCurrentSpeed());
	}
	
	public ValuePair getAverageSpeed(Trip trip) {
		return formatSpeed(trip.getAverageSpeed());
	}
	
	public ValuePair getMaxSpeed(Trip trip) {
		return formatSpeed(trip.getMaxSpeed());
	}
	
	public ValuePair getAltitude(Trip trip) {
		return formatAltitude(trip.getAltitude());
	}
	
	public ValuePair getDistance(Trip trip) {
		Float converted = Unit.convertDistance(trip.getDistance(), Trip.UNIT_DISTANCE, distanceUnit);
		String first = formatNoValue(formatter.formatDistance(converted, distanceUnit));
		String second = distanceMapper.getString(distanceUnit);
		return new ValuePair(first, second);
	}
	
	public ValuePair getTime(Trip trip) {
		return formatDuration(trip.getTime());
	}
	
	public ValuePair getElevationAsc(Trip trip) {
		return formatAltitude(trip.getElevationAsc());
	}
	
	public ValuePair getElevationDesc(Trip trip) {
		return formatAltitude(trip.getElevationDesc());
	}
	
	public ValuePair getMinAltitude(Trip trip) {
		return formatAltitude(trip.getMinAltitude());
	}
	
	public ValuePair getMaxAltitude(Trip trip) {
		return formatAltitude(trip.getMaxAltitude());
	}
	
	public ValuePair getTotalTime(Trip trip) {
		return formatDuration(trip.getTotalTime());
	}
	
	public ValuePair getCalories(HealthCalculator healthCalculator) {
		Float converted = Unit.convertEnergy(healthCalculator.getCaloriesBurned(), HealthCalculator.UNIT_ENERGY,
				energyUnit);
		String first = formatNoValue(formatter.formatEnergy(converted, energyUnit));
		String second = energyMapper.getString(energyUnit);
		return new ValuePair(first, second);
	}
	
	public ValuePair getFatBurned(HealthCalculator healthCalculator) {
		Float converted = Unit.convertWeight(healthCalculator.getFatBurned(), HealthCalculator.UNIT_WEIGHT, weightUnit);
		String first = formatNoValue(formatter.formatWeight(converted, weightUnit));
		String second = weightMapper.getString(weightUnit);
		return new ValuePair(first, second);
	}
	
	public ValuePair getOxygenConsumed(HealthCalculator healthCalculator) {
		Float converted = Unit.convertVolume(healthCalculator.getOxygenConsumed(), HealthCalculator.UNIT_VOLUME,
				volumeUnit);
		String first = formatNoValue(formatter.formatVolume(converted, volumeUnit));
		String second = volumeMapper.getString(volumeUnit);
		return new ValuePair(first, second);
	}
	
	public ValuePair getPaceNetto(Trip trip) {
		return formatPace(trip.getPaceNetto());
	}
	
	public ValuePair getPaceBrutto(Trip trip) {
		return formatPace(trip.getPaceBrutto());
	}
	
	public ValuePair getBearing(Trip trip) {
		Float converted = Unit.convertAngle(trip.getBearing(), Trip.UNIT_ANGLE, bearingUnit);
		String first = formatNoValue(formatter.formatAngle(converted, bearingUnit));
		String second = angleMapper.getString(bearingUnit);
		return new ValuePair(first, second);
	}
	
	public ValuePair getSpeedBrutto(Trip trip) {
		return formatSpeed(trip.getSpeedBrutto());
	}
	
	public ValuePair getSlope(Trip trip) {
		Float converted = Unit.convertAngle(trip.getSlope(), Trip.UNIT_ANGLE, slopeUnit);
		String first = formatNoValue(formatter.formatAngle(converted, slopeUnit));
		String second = angleMapper.getString(slopeUnit);
		return new ValuePair(first, second);
	}
	
	public ValuePair getPower(HealthCalculator healthCalculator) {
		Float converted = Unit.convertPower(healthCalculator.getPower(), HealthCalculator.UNIT_POWER, powerUnit);
		String first = formatNoValue(formatter.formatPower(converted, powerUnit));
		String second = powerMapper.getString(powerUnit);
		return new ValuePair(first, second);
	}
	
	public ValuePair getInitialAltitude(Trip trip) {
		return formatAltitude(trip.getInitialAltitude());
	}
	
	public ValuePair getFinalAltitude(Trip trip) {
		return formatAltitude(trip.getFinalAltitude());
	}
	
	public ValuePair getStartTime(Trip trip) {
		return formatDate(trip.getStartTime());
	}
	
	public ValuePair getEndTime(Trip trip) {
		return formatDate(trip.getEndTime());
	}
	
	public ValuePair getTimeFromStart(Trip trip) {
		return formatDuration(trip.getTimeFromStart());
	}
}
