/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class CircularBuffer<T> {
	private final LinkedList<T> container = new LinkedList<T>();
	private final int capacity;
	
	public CircularBuffer(int capacity) {
		this.capacity = capacity;
	}
	
	public T add(T object) {
		T result = null;
		if(container.size() == capacity)
			result = container.removeLast();
		container.addFirst(object);
		return result;
	}
	
	public int size() {
		return container.size();
	}
	
	public T getFirst() {
		return container.getFirst();
	}
	
	public T getLast() {
		return container.getLast();
	}
	
	public ListIterator<T> listIterator(int location) {
		return container.listIterator(location);
	}
	
	public Iterator<T> iterator() {
		return container.iterator();
	}
	
	public void clear() {
		container.clear();
	}
}
