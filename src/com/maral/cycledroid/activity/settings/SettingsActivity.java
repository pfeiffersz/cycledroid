/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.DonateActivity;
import com.maral.cycledroid.activity.HelpActivity;
import com.maral.cycledroid.activity.settings.Settings.SettingType;
import com.maral.cycledroid.activity.settings.Settings.UnitsSystemType;
import com.maral.cycledroid.mapper.CachedStringMapper;
import com.maral.cycledroid.mapper.StringMapper;
import com.maral.cycledroid.ui.ActivityManager;
import com.maral.cycledroid.ui.ActivityManagerSettings;
import com.maral.cycledroid.widget.ValidatablePreference;
import com.maral.cycledroid.widget.ValidatablePreference.ValueValidator;

public final class SettingsActivity extends PreferenceActivity {
	private class Controller implements OnSharedPreferenceChangeListener {
		public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
			settings.onSharedPreferenceChanged(key);
			setUnitsState();
		}
	}
	
	private final Controller controller = new Controller();
	
	private StringMapper<SettingType> keys;
	private Settings settings;
	private ActivityManager activityManager;
	
	private static final ValueValidator NUMBER_VALIDATOR = new ValueValidator() {
		public boolean validate(Editable s) {
			try {
				Integer.parseInt(s.toString());
			} catch(NumberFormatException exception) {
				return false;
			}
			return true;
		}
	};
	
	private static final ValueValidator YEAR_VALIDATOR = new ValueValidator() {
		public boolean validate(Editable s) {
			try {
				int year = Integer.parseInt(s.toString());
				return year >= 1850 && year <= 2014; // change this next year :)
			} catch(NumberFormatException exception) {
				return false;
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
		
		// validate birth year and weight
		keys = new CachedStringMapper<SettingType>(KeyResourceMapper.getInstance(), this);
		ValidatablePreference caloriesBirthYear = (ValidatablePreference)findPreference(keys
				.getString(SettingType.CALORIES_BIRTH_YEAR));
		caloriesBirthYear.setValidator(YEAR_VALIDATOR);
		ValidatablePreference caloriesWeight = (ValidatablePreference)findPreference(keys
				.getString(SettingType.CALORIES_WEIGHT));
		caloriesWeight.setValidator(NUMBER_VALIDATOR);
		
		settings = SettingsSystem.getInstance(this);
		setUnitsState();
		
		activityManager = new ActivityManagerSettings(this, settings);
		
		PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(controller);
	}
	
	private void setUnitsState() {
		// this method enables/disables units preferences for user according to value of system of units
		// (custom -> enable, other -> disable) 
		setSingleUnitState(SettingType.ALTITUDE_UNIT);
		setSingleUnitState(SettingType.DISTANCE_UNIT);
		setSingleUnitState(SettingType.PACE_UNIT);
		setSingleUnitState(SettingType.SPEED_UNIT);
		setSingleUnitState(SettingType.VOLUME_UNIT);
		setSingleUnitState(SettingType.WEIGHT_UNIT);
	}
	
	private void setSingleUnitState(SettingType setting) {
		boolean enabled = settings.getUnitsSystem() == UnitsSystemType.CUSTOM;
		findPreference(keys.getString(setting)).setEnabled(enabled);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		activityManager.detachActivity(this);
		PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(controller);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_help, menu);
		inflater.inflate(R.menu.option_donate, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.donate).setVisible(!settings.getRemoveDonate());
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.help:
				(new HelpActivity.HelpOpener(this, getString(R.string.help_file))).openHelp();
				return true;
			case R.id.donate:
				startActivity(new Intent(this, DonateActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
