/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.settings;

import java.lang.ref.WeakReference;

import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.settings.Settings.Orientation;
import com.maral.cycledroid.activity.settings.Settings.Sex;
import com.maral.cycledroid.activity.settings.Settings.SortingOrder;
import com.maral.cycledroid.activity.settings.Settings.UnitsSystemType;
import com.maral.cycledroid.mapper.ResourceMapper;
import com.maral.cycledroid.model.Unit.Altitude;
import com.maral.cycledroid.model.Unit.Distance;
import com.maral.cycledroid.model.Unit.Pace;
import com.maral.cycledroid.model.Unit.Speed;
import com.maral.cycledroid.model.Unit.Volume;
import com.maral.cycledroid.model.Unit.Weight;

final class ValueResourceMapper {
	private ValueResourceMapper() {} // prevent from creating
	
	public static final class CaloriesSexMapper implements ResourceMapper<Sex> {
		private static WeakReference<CaloriesSexMapper> instance = new WeakReference<CaloriesSexMapper>(null);
		
		private CaloriesSexMapper() {}
		
		public static CaloriesSexMapper getInstance() {
			CaloriesSexMapper reference = instance.get();
			if(reference == null) {
				reference = new CaloriesSexMapper();
				instance = new WeakReference<CaloriesSexMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Sex sex) {
			switch(sex) {
				case FEMALE:
					return R.string.calories_sex_female_value;
				case MALE:
					return R.string.calories_sex_male_value;
			}
			throw new IllegalArgumentException("Sex " + sex + " is not supported.");
		}
	}
	
	public static final class OrientationMapper implements ResourceMapper<Orientation> {
		private static WeakReference<OrientationMapper> instance = new WeakReference<OrientationMapper>(null);
		
		private OrientationMapper() {}
		
		public static OrientationMapper getInstance() {
			OrientationMapper reference = instance.get();
			if(reference == null) {
				reference = new OrientationMapper();
				instance = new WeakReference<OrientationMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Orientation orientation) {
			switch(orientation) {
				case LANDSCAPE:
					return R.string.orientation_landscape_value;
				case PORTRAIT:
					return R.string.orientation_portrait_value;
				case SENSOR:
					return R.string.orientation_sensor_value;
			}
			throw new IllegalArgumentException("Orientation " + orientation + " is not supported.");
		}
	}
	
	public static final class SortingOrderMapper implements ResourceMapper<SortingOrder> {
		private static WeakReference<SortingOrderMapper> instance = new WeakReference<SortingOrderMapper>(null);
		
		private SortingOrderMapper() {}
		
		public static SortingOrderMapper getInstance() {
			SortingOrderMapper reference = instance.get();
			if(reference == null) {
				reference = new SortingOrderMapper();
				instance = new WeakReference<SortingOrderMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(SortingOrder order) {
			switch(order) {
				case ALPH_ASC:
					return R.string.trips_sorting_alph_asc_value;
				case ALPH_DESC:
					return R.string.trips_sorting_alph_desc_value;
				case CHRON_ASC:
					return R.string.trips_sorting_chron_asc_value;
				case CHRON_DESC:
					return R.string.trips_sorting_chron_desc_value;
			}
			throw new IllegalArgumentException("Sorting order " + order + " is not supported.");
		}
	}
	
	public static final class UnitsSystemMapper implements ResourceMapper<UnitsSystemType> {
		private static WeakReference<UnitsSystemMapper> instance = new WeakReference<UnitsSystemMapper>(null);
		
		private UnitsSystemMapper() {}
		
		public static UnitsSystemMapper getInstance() {
			UnitsSystemMapper reference = instance.get();
			if(reference == null) {
				reference = new UnitsSystemMapper();
				instance = new WeakReference<UnitsSystemMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(UnitsSystemType system) {
			switch(system) {
				case CUSTOM:
					return R.string.units_system_custom_value;
				case IMPERIAL:
					return R.string.units_system_imperial_value;
				case METRIC:
					return R.string.units_system_metric_value;
			}
			throw new IllegalArgumentException("System of units " + system + " is not supported.");
		}
	}
	
	public static final class AltitudeUnitMapper implements ResourceMapper<Altitude> {
		private static WeakReference<AltitudeUnitMapper> instance = new WeakReference<AltitudeUnitMapper>(null);
		
		private AltitudeUnitMapper() {}
		
		public static AltitudeUnitMapper getInstance() {
			AltitudeUnitMapper reference = instance.get();
			if(reference == null) {
				reference = new AltitudeUnitMapper();
				instance = new WeakReference<AltitudeUnitMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Altitude unit) {
			switch(unit) {
				case FT:
					return R.string.unit_altitude_ft_value;
				case KM:
					return R.string.unit_altitude_km_value;
				case M:
					return R.string.unit_altitude_m_value;
				case MI:
					return R.string.unit_altitude_mi_value;
				case NMI:
					return R.string.unit_altitude_nmi_value;
			}
			throw new IllegalArgumentException("Altitude unit " + unit + " is not supported.");
		}
	}
	
	public static final class DistanceUnitMapper implements ResourceMapper<Distance> {
		private static WeakReference<DistanceUnitMapper> instance = new WeakReference<DistanceUnitMapper>(null);
		
		private DistanceUnitMapper() {}
		
		public static DistanceUnitMapper getInstance() {
			DistanceUnitMapper reference = instance.get();
			if(reference == null) {
				reference = new DistanceUnitMapper();
				instance = new WeakReference<DistanceUnitMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Distance unit) {
			switch(unit) {
				case FT:
					return R.string.unit_distance_ft_value;
				case KM:
					return R.string.unit_distance_km_value;
				case M:
					return R.string.unit_distance_m_value;
				case MI:
					return R.string.unit_distance_mi_value;
				case NMI:
					return R.string.unit_distance_nmi_value;
			}
			throw new IllegalArgumentException("Distance unit " + unit + " is not supported.");
		}
	}
	
	public static final class PaceUnitMapper implements ResourceMapper<Pace> {
		private static WeakReference<PaceUnitMapper> instance = new WeakReference<PaceUnitMapper>(null);
		
		private PaceUnitMapper() {}
		
		public static PaceUnitMapper getInstance() {
			PaceUnitMapper reference = instance.get();
			if(reference == null) {
				reference = new PaceUnitMapper();
				instance = new WeakReference<PaceUnitMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Pace unit) {
			switch(unit) {
				case S_PER_KM:
					return R.string.unit_pace_tpkm_value;
				case S_PER_MI:
					return R.string.unit_pace_tpmi_value;
				case S_PER_NMI:
					return R.string.unit_pace_tpnmi_value;
				case MS_PER_FT:
					return R.string.unit_pace_tpft_value;
				case MS_PER_M:
					return R.string.unit_pace_tpm_value;
			}
			throw new IllegalArgumentException("Pace unit " + unit + " is not supported.");
		}
	}
	
	public static final class SpeedUnitMapper implements ResourceMapper<Speed> {
		private static WeakReference<SpeedUnitMapper> instance = new WeakReference<SpeedUnitMapper>(null);
		
		private SpeedUnitMapper() {}
		
		public static SpeedUnitMapper getInstance() {
			SpeedUnitMapper reference = instance.get();
			if(reference == null) {
				reference = new SpeedUnitMapper();
				instance = new WeakReference<SpeedUnitMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Speed unit) {
			switch(unit) {
				case FTS:
					return R.string.unit_speed_fts_value;
				case KMH:
					return R.string.unit_speed_kmh_value;
				case KN:
					return R.string.unit_speed_kn_value;
				case MPH:
					return R.string.unit_speed_mph_value;
				case MS:
					return R.string.unit_speed_ms_value;
			}
			throw new IllegalArgumentException("Speed unit " + unit + " is not supported.");
		}
	}
	
	public static final class VolumeUnitMapper implements ResourceMapper<Volume> {
		private static WeakReference<VolumeUnitMapper> instance = new WeakReference<VolumeUnitMapper>(null);
		
		private VolumeUnitMapper() {}
		
		public static VolumeUnitMapper getInstance() {
			VolumeUnitMapper reference = instance.get();
			if(reference == null) {
				reference = new VolumeUnitMapper();
				instance = new WeakReference<VolumeUnitMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Volume unit) {
			switch(unit) {
				case GAL:
					return R.string.unit_volume_gal_value;
				case L:
					return R.string.unit_volume_l_value;
			}
			throw new IllegalArgumentException("Volume unit " + unit + " is not supported.");
		}
	}
	
	public static final class WeightUnitMapper implements ResourceMapper<Weight> {
		private static WeakReference<WeightUnitMapper> instance = new WeakReference<WeightUnitMapper>(null);
		
		private WeightUnitMapper() {}
		
		public static WeightUnitMapper getInstance() {
			WeightUnitMapper reference = instance.get();
			if(reference == null) {
				reference = new WeightUnitMapper();
				instance = new WeakReference<WeightUnitMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Weight unit) {
			switch(unit) {
				case G:
					return R.string.unit_weight_g_value;
				case OZ:
					return R.string.unit_weight_oz_value;
			}
			throw new IllegalArgumentException("Weight unit " + unit + " is not supported.");
		}
	}
}
