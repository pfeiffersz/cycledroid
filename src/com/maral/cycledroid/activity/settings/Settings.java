/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.settings;

import java.util.Observable;

import com.maral.cycledroid.model.Unit.Altitude;
import com.maral.cycledroid.model.Unit.Distance;
import com.maral.cycledroid.model.Unit.Pace;
import com.maral.cycledroid.model.Unit.Speed;
import com.maral.cycledroid.model.Unit.Volume;
import com.maral.cycledroid.model.Unit.Weight;

public abstract class Settings extends Observable {
	public static enum SettingType {
		UNITS_SYSTEM, SPEED_UNIT, DISTANCE_UNIT, ALTITUDE_UNIT, PACE_UNIT, WEIGHT_UNIT, VOLUME_UNIT,
		GRAPH_ALTITUDE_ZERO, LOCK_SCREEN, TRIPS_SORTING, REMOVE_DONATE, ORIENTATION, AUTO_START_TRACKING, CALORIES_SEX,
		CALORIES_WEIGHT, CALORIES_AGE, CALORIES_BIRTH_YEAR, FACEBOOK_MAP, FACEBOOK_DURATION, FACEBOOK_SPEED,
		FACEBOOK_CALORIES, FACEBOOK_PACE, FACEBOOK_TRIP_DATE,
	}
	
	public static enum Sex {
		MALE, FEMALE,
	}
	
	public static enum SortingOrder {
		ALPH_ASC, ALPH_DESC, CHRON_ASC, CHRON_DESC,
	}
	
	public static enum UnitsSystemType {
		IMPERIAL, METRIC, CUSTOM,
	}
	
	public enum Orientation {
		LANDSCAPE, PORTRAIT, SENSOR,
	}
	
	protected abstract void onSharedPreferenceChanged(String key);
	
	public abstract UnitsSystemType getUnitsSystem();
	
	public abstract Altitude getAltitudeUnit();
	
	public abstract Distance getDistanceUnit();
	
	public abstract Speed getSpeedUnit();
	
	public abstract Pace getPaceUnit();
	
	public abstract Weight getWeightUnit();
	
	public abstract Volume getVolumeUnit();
	
	public abstract boolean getGraphAltitudeZero();
	
	public abstract boolean getLockScreen();
	
	public abstract SortingOrder getTripsSorting();
	
	public abstract boolean getRemoveDonate();
	
	public abstract Orientation getOrientation();
	
	public abstract boolean getAutoStartTracking();
	
	public abstract Sex getSex();
	
	public abstract float getWeight(); // in kilograms
	
	public abstract int getBirthYear();
	
	public abstract boolean getFacebookMap();
	
	public abstract boolean getFacebookDuration();
	
	public abstract boolean getFacebookSpeed();
	
	public abstract boolean getFacebookCalories();
	
	public abstract boolean getFacebookPace();
	
	public abstract boolean getFacebookTripDate();
}
