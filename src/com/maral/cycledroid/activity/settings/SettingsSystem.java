/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.settings;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.settings.CommonSystems.ImperialSystem;
import com.maral.cycledroid.activity.settings.CommonSystems.MetricSystem;
import com.maral.cycledroid.activity.settings.ValueResourceMapper.AltitudeUnitMapper;
import com.maral.cycledroid.activity.settings.ValueResourceMapper.CaloriesSexMapper;
import com.maral.cycledroid.activity.settings.ValueResourceMapper.DistanceUnitMapper;
import com.maral.cycledroid.activity.settings.ValueResourceMapper.OrientationMapper;
import com.maral.cycledroid.activity.settings.ValueResourceMapper.PaceUnitMapper;
import com.maral.cycledroid.activity.settings.ValueResourceMapper.SortingOrderMapper;
import com.maral.cycledroid.activity.settings.ValueResourceMapper.SpeedUnitMapper;
import com.maral.cycledroid.activity.settings.ValueResourceMapper.UnitsSystemMapper;
import com.maral.cycledroid.activity.settings.ValueResourceMapper.VolumeUnitMapper;
import com.maral.cycledroid.activity.settings.ValueResourceMapper.WeightUnitMapper;
import com.maral.cycledroid.mapper.CachedOnceStringMapper;
import com.maral.cycledroid.model.Unit.Altitude;
import com.maral.cycledroid.model.Unit.Distance;
import com.maral.cycledroid.model.Unit.Pace;
import com.maral.cycledroid.model.Unit.Speed;
import com.maral.cycledroid.model.Unit.Volume;
import com.maral.cycledroid.model.Unit.Weight;

public class SettingsSystem extends Settings {
	private final class CustomSystem implements UnitsSystem {
		public Altitude getAltitudeUnit() {
			return altitudeUnit;
		}
		
		public Distance getDistanceUnit() {
			return distanceUnit;
		}
		
		public Pace getPaceUnit() {
			return paceUnit;
		}
		
		public Speed getSpeedUnit() {
			return speedUnit;
		}
		
		public Volume getVolumeUnit() {
			return volumeUnit;
		}
		
		public Weight getWeightUnit() {
			return weightUnit;
		}
	}
	
	private final Map<UnitsSystemType, UnitsSystem> systems = new HashMap<UnitsSystemType, UnitsSystem>();
	
	private static SettingsSystem instance = null;
	
	private final SharedPreferences preferences;
	private final CachedOnceStringMapper<SettingType> keyMapper;
	private final CachedOnceStringMapper<Sex> sexMapper;
	private final CachedOnceStringMapper<Orientation> orientationMapper;
	private final CachedOnceStringMapper<SortingOrder> sortingMapper;
	private final CachedOnceStringMapper<UnitsSystemType> unitsSystemMapper;
	private final CachedOnceStringMapper<Altitude> altitudeMapper;
	private final CachedOnceStringMapper<Distance> distanceMapper;
	private final CachedOnceStringMapper<Pace> paceMapper;
	private final CachedOnceStringMapper<Speed> speedMapper;
	private final CachedOnceStringMapper<Volume> volumeMapper;
	private final CachedOnceStringMapper<Weight> weightMapper;
	
	private UnitsSystemType unitsSystem = null;
	private Altitude altitudeUnit = null;
	private Distance distanceUnit = null;
	private Speed speedUnit = null;
	private Weight weightUnit = null;
	private Volume volumeUnit = null;
	private Pace paceUnit = null;
	private boolean graphAltitudeZero = true;
	private boolean lockScreen = false;
	private SortingOrder tripsSorting = null;
	private boolean removeDonate = false;
	private Orientation orientation = Orientation.SENSOR;
	private boolean autoStartTracking = false;
	private Sex caloriesSex = Sex.MALE;
	private float caloriesWeight;
	private int caloriesBirthYear;
	private boolean facebookMap = true;
	private boolean facebookDuration = true;
	private boolean facebookSpeed = true;
	private boolean facebookCalories = false;
	private boolean facebookPace = false;
	private boolean facebookTripDate = true;
	
	private SettingsSystem(Context context) {
		// create map of systems of units
		systems.put(UnitsSystemType.CUSTOM, new CustomSystem());
		systems.put(UnitsSystemType.IMPERIAL, ImperialSystem.getInstance());
		systems.put(UnitsSystemType.METRIC, MetricSystem.getInstance());
		
		// cache string resources corresponding to keys and values
		keyMapper = new CachedOnceStringMapper<SettingType>(KeyResourceMapper.getInstance(), context,
				SettingType.values());
		sexMapper = new CachedOnceStringMapper<Sex>(CaloriesSexMapper.getInstance(), context, Sex.values());
		orientationMapper = new CachedOnceStringMapper<Orientation>(OrientationMapper.getInstance(), context,
				Orientation.values());
		sortingMapper = new CachedOnceStringMapper<SortingOrder>(SortingOrderMapper.getInstance(), context,
				SortingOrder.values());
		unitsSystemMapper = new CachedOnceStringMapper<UnitsSystemType>(UnitsSystemMapper.getInstance(), context,
				UnitsSystemType.values());
		altitudeMapper = new CachedOnceStringMapper<Altitude>(AltitudeUnitMapper.getInstance(), context,
				Altitude.values());
		distanceMapper = new CachedOnceStringMapper<Distance>(DistanceUnitMapper.getInstance(), context,
				Distance.values());
		paceMapper = new CachedOnceStringMapper<Pace>(PaceUnitMapper.getInstance(), context, Pace.values());
		speedMapper = new CachedOnceStringMapper<Speed>(SpeedUnitMapper.getInstance(), context, Speed.values());
		volumeMapper = new CachedOnceStringMapper<Volume>(VolumeUnitMapper.getInstance(), context, Volume.values());
		weightMapper = new CachedOnceStringMapper<Weight>(WeightUnitMapper.getInstance(), context, Weight.values());
		
		// handle update from version without system of units in settings
		preferences = PreferenceManager.getDefaultSharedPreferences(context);
		if(preferences.getString(keyMapper.getString(SettingType.UNITS_SYSTEM), null) == null) {
			if(preferences.getString(keyMapper.getString(SettingType.ALTITUDE_UNIT), null) == null) {
				Editor editor = preferences.edit();
				editor.putString(keyMapper.getString(SettingType.UNITS_SYSTEM),
						context.getString(R.string.units_system_metric_value));
				editor.commit();
			}
		}
		PreferenceManager.setDefaultValues(context, R.xml.settings, true);
		
		for(SettingType setting : SettingType.values())
			settingUpdate(setting);
	}
	
	public static final SettingsSystem getInstance(Context context) {
		if(instance == null)
			instance = new SettingsSystem(context);
		return instance;
	}
	
	private void notifyAllObservers(SettingType setting) {
		setChanged();
		notifyObservers(setting);
	}
	
	@Override
	protected void onSharedPreferenceChanged(String key) {
		SettingType setting = keyMapper.reverseMapping(key);
		settingUpdate(setting);
		notifyAllObservers(setting);
		if(setting == SettingType.UNITS_SYSTEM) {
			notifyAllObservers(SettingType.ALTITUDE_UNIT);
			notifyAllObservers(SettingType.DISTANCE_UNIT);
			notifyAllObservers(SettingType.PACE_UNIT);
			notifyAllObservers(SettingType.SPEED_UNIT);
			notifyAllObservers(SettingType.VOLUME_UNIT);
			notifyAllObservers(SettingType.WEIGHT_UNIT);
		}
	}
	
	@Override
	public UnitsSystemType getUnitsSystem() {
		return unitsSystem;
	}
	
	@Override
	public Altitude getAltitudeUnit() {
		return systems.get(unitsSystem).getAltitudeUnit();
	}
	
	@Override
	public Distance getDistanceUnit() {
		return systems.get(unitsSystem).getDistanceUnit();
	}
	
	@Override
	public Speed getSpeedUnit() {
		return systems.get(unitsSystem).getSpeedUnit();
	}
	
	@Override
	public Pace getPaceUnit() {
		return systems.get(unitsSystem).getPaceUnit();
	}
	
	@Override
	public Weight getWeightUnit() {
		return systems.get(unitsSystem).getWeightUnit();
	}
	
	@Override
	public Volume getVolumeUnit() {
		return systems.get(unitsSystem).getVolumeUnit();
	}
	
	@Override
	public boolean getGraphAltitudeZero() {
		return graphAltitudeZero;
	}
	
	@Override
	public boolean getLockScreen() {
		return lockScreen;
	}
	
	@Override
	public SortingOrder getTripsSorting() {
		return tripsSorting;
	}
	
	@Override
	public boolean getRemoveDonate() {
		return removeDonate;
	}
	
	@Override
	public Orientation getOrientation() {
		return orientation;
	}
	
	@Override
	public boolean getAutoStartTracking() {
		return autoStartTracking;
	}
	
	@Override
	public Sex getSex() {
		return caloriesSex;
	}
	
	@Override
	public float getWeight() {
		return caloriesWeight;
	}
	
	@Override
	public int getBirthYear() {
		return caloriesBirthYear;
	}
	
	@Override
	public boolean getFacebookMap() {
		return facebookMap;
	}
	
	@Override
	public boolean getFacebookDuration() {
		return facebookDuration;
	}
	
	@Override
	public boolean getFacebookSpeed() {
		return facebookSpeed;
	}
	
	@Override
	public boolean getFacebookCalories() {
		return facebookCalories;
	}
	
	@Override
	public boolean getFacebookPace() {
		return facebookPace;
	}
	
	@Override
	public boolean getFacebookTripDate() {
		return facebookTripDate;
	}
	
	private void settingUpdate(SettingType setting) {
		switch(setting) {
			case ALTITUDE_UNIT:
				updateAltitudeUnit();
				break;
			case AUTO_START_TRACKING:
				updateAutoStartTracking();
				break;
			case CALORIES_AGE:
			case CALORIES_BIRTH_YEAR:
				updateCaloriesBirthYear();
				break;
			case CALORIES_SEX:
				updateCaloriesSex();
				break;
			case CALORIES_WEIGHT:
				updateCaloriesWeight();
				break;
			case DISTANCE_UNIT:
				updateDistanceUnit();
				break;
			case GRAPH_ALTITUDE_ZERO:
				updateGraphAltitudeZero();
				break;
			case LOCK_SCREEN:
				updateLockScreen();
				break;
			case ORIENTATION:
				updateOrientation();
				break;
			case PACE_UNIT:
				updatePaceUnit();
				break;
			case REMOVE_DONATE:
				updateRemoveDonate();
				break;
			case SPEED_UNIT:
				updateSpeedUnit();
				break;
			case TRIPS_SORTING:
				updateTripsSorting();
				break;
			case UNITS_SYSTEM:
				updateUnitsSystem();
				break;
			case VOLUME_UNIT:
				updateVolumeUnit();
				break;
			case WEIGHT_UNIT:
				updateWeightUnit();
				break;
			case FACEBOOK_MAP:
				updateFacebookMap();
				break;
			case FACEBOOK_DURATION:
				updateFacebookDuration();
				break;
			case FACEBOOK_SPEED:
				updateFacebookSpeed();
				break;
			case FACEBOOK_CALORIES:
				updateFacebookCalories();
				break;
			case FACEBOOK_PACE:
				updateFacebookPace();
				break;
			case FACEBOOK_TRIP_DATE:
				updateFacebookTripDate();
				break;
		}
	}
	
	private void updateAltitudeUnit() {
		String key = keyMapper.getString(SettingType.ALTITUDE_UNIT);
		altitudeUnit = altitudeMapper.reverseMapping(preferences.getString(key, null));
	}
	
	private void updateDistanceUnit() {
		String key = keyMapper.getString(SettingType.DISTANCE_UNIT);
		distanceUnit = distanceMapper.reverseMapping(preferences.getString(key, null));
	}
	
	private void updateSpeedUnit() {
		String key = keyMapper.getString(SettingType.SPEED_UNIT);
		speedUnit = speedMapper.reverseMapping(preferences.getString(key, null));
	}
	
	private void updateUnitsSystem() {
		String key = keyMapper.getString(SettingType.UNITS_SYSTEM);
		unitsSystem = unitsSystemMapper.reverseMapping(preferences.getString(key, null));
	}
	
	private void updateWeightUnit() {
		String key = keyMapper.getString(SettingType.WEIGHT_UNIT);
		weightUnit = weightMapper.reverseMapping(preferences.getString(key, null));
	}
	
	private void updateVolumeUnit() {
		String key = keyMapper.getString(SettingType.VOLUME_UNIT);
		volumeUnit = volumeMapper.reverseMapping(preferences.getString(key, null));
	}
	
	private void updatePaceUnit() {
		String key = keyMapper.getString(SettingType.PACE_UNIT);
		paceUnit = paceMapper.reverseMapping(preferences.getString(key, null));
	}
	
	private void updateGraphAltitudeZero() {
		String key = keyMapper.getString(SettingType.GRAPH_ALTITUDE_ZERO);
		graphAltitudeZero = preferences.getBoolean(key, true);
	}
	
	private void updateLockScreen() {
		String key = keyMapper.getString(SettingType.LOCK_SCREEN);
		lockScreen = preferences.getBoolean(key, false);
	}
	
	private void updateTripsSorting() {
		String key = keyMapper.getString(SettingType.TRIPS_SORTING);
		tripsSorting = sortingMapper.reverseMapping(preferences.getString(key, null));
	}
	
	private void updateRemoveDonate() {
		String key = keyMapper.getString(SettingType.REMOVE_DONATE);
		removeDonate = preferences.getBoolean(key, false);
	}
	
	private void updateOrientation() {
		String key = keyMapper.getString(SettingType.ORIENTATION);
		orientation = orientationMapper.reverseMapping(preferences.getString(key, null));
	}
	
	private void updateAutoStartTracking() {
		String key = keyMapper.getString(SettingType.AUTO_START_TRACKING);
		autoStartTracking = preferences.getBoolean(key, false);
	}
	
	private void updateCaloriesSex() {
		String key = keyMapper.getString(SettingType.CALORIES_SEX);
		caloriesSex = sexMapper.reverseMapping(preferences.getString(key, null));
	}
	
	private void updateCaloriesWeight() {
		String key = keyMapper.getString(SettingType.CALORIES_WEIGHT);
		caloriesWeight = Float.parseFloat(preferences.getString(key, null));
	}
	
	private void updateCaloriesBirthYear() {
		String key = keyMapper.getString(SettingType.CALORIES_BIRTH_YEAR);
		caloriesBirthYear = Integer.parseInt(preferences.getString(key, null));
	}
	
	private void updateFacebookMap() {
		String key = keyMapper.getString(SettingType.FACEBOOK_MAP);
		facebookMap = preferences.getBoolean(key, true);
	}
	
	private void updateFacebookDuration() {
		String key = keyMapper.getString(SettingType.FACEBOOK_DURATION);
		facebookDuration = preferences.getBoolean(key, true);
	}
	
	private void updateFacebookSpeed() {
		String key = keyMapper.getString(SettingType.FACEBOOK_SPEED);
		facebookSpeed = preferences.getBoolean(key, true);
	}
	
	private void updateFacebookCalories() {
		String key = keyMapper.getString(SettingType.FACEBOOK_CALORIES);
		facebookCalories = preferences.getBoolean(key, false);
	}
	
	private void updateFacebookPace() {
		String key = keyMapper.getString(SettingType.FACEBOOK_PACE);
		facebookPace = preferences.getBoolean(key, false);
	}
	
	private void updateFacebookTripDate() {
		String key = keyMapper.getString(SettingType.FACEBOOK_TRIP_DATE);
		facebookTripDate = preferences.getBoolean(key, true);
	}
}
