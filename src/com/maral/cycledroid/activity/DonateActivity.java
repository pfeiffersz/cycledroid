/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.maral.cycledroid.AppInfo;
import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.SettingsActivity;
import com.maral.cycledroid.activity.settings.SettingsSystem;
import com.maral.cycledroid.billing.IabHelper;
import com.maral.cycledroid.billing.IabHelper.OnConsumeFinishedListener;
import com.maral.cycledroid.billing.IabHelper.OnIabPurchaseFinishedListener;
import com.maral.cycledroid.billing.IabHelper.OnIabSetupFinishedListener;
import com.maral.cycledroid.billing.IabHelper.QueryInventoryFinishedListener;
import com.maral.cycledroid.billing.IabResult;
import com.maral.cycledroid.billing.Inventory;
import com.maral.cycledroid.billing.Purchase;
import com.maral.cycledroid.ui.ActivityManager;
import com.maral.cycledroid.ui.ActivityManagerSettings;

public final class DonateActivity extends Activity {
	private final class Controller implements OnIabSetupFinishedListener, OnIabPurchaseFinishedListener,
			OnConsumeFinishedListener, OnClickListener, QueryInventoryFinishedListener {
		private void finishInitialization() {
			findViewById(R.id.donate_financially).setEnabled(true);
		}
		
		@Override
		public void onIabSetupFinished(IabResult result) {
			if(result.isSuccess()) {
				canPurchase = true;
				iabHelper.queryInventoryAsync(this);
			} else {
				errorMessage = result.getMessage();
				finishInitialization();
			}
		}
		
		@Override
		public void onIabPurchaseFinished(IabResult result, Purchase info) {
			if(result.isSuccess()) {
				appInfo.setDonated(true);
				iabHelper.consumeAsync(info, this);
			}
		}
		
		@Override
		public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
			if(result.isSuccess()) {
				if(inventory.hasPurchase(DONATE_SKU))
					iabHelper.consumeAsync(inventory.getPurchase(DONATE_SKU), this);
				else
					finishInitialization();
			} else {
				errorMessage = result.getMessage();
				finishInitialization();
			}
		}
		
		@Override
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			if(donating)
				donating = false;
			else {
				if(result.isFailure())
					errorMessage = result.getMessage();
				finishInitialization();
			}
		}
		
		@Override
		public void onClick(View view) {
			switch(view.getId()) {
				case R.id.donate_financially:
					if(!canPurchase)
						showDialog(DialogType.ERROR_IAB.ordinal());
					else if(appInfo.getDonated())
						showDialog(DialogType.DONATE_AGAIN.ordinal());
					else
						performDonate();
					break;
				case R.id.rate:
					try {
						startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="
								+ getPackageName())));
					} catch(ActivityNotFoundException exception) {
						startActivity(new Intent(Intent.ACTION_VIEW,
								Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
					}
					break;
				case R.id.translate:
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setData(Uri.parse("http://crowdin.net/project/cycledroid"));
					startActivity(intent);
					break;
			}
		}
	}
	
	private static enum DialogType {
		ERROR_IAB, DONATE_AGAIN,
	}
	
	private final Controller controller = new Controller();
	
	private static final String DONATE_SKU = "cycledroid_donate";
	
	private static final int BUY_REQUEST_CODE = 341; // to differentiate from other responses
	
	private Settings settings;
	private ActivityManager activityManager;
	
	private String errorMessage = "";
	private IabHelper iabHelper = null;
	private boolean canPurchase = false;
	private boolean donating = false;
	private AppInfo appInfo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.donate);
		findViewById(R.id.donate_financially).setOnClickListener(controller);
		findViewById(R.id.donate_financially).setEnabled(false); // disable this button until we initialize the in-app billing
		findViewById(R.id.rate).setOnClickListener(controller);
		findViewById(R.id.translate).setOnClickListener(controller);
		((TextView)findViewById(R.id.translate_text)).setMovementMethod(LinkMovementMethod.getInstance()); // make link clickable
		appInfo = new AppInfo(this);
		settings = SettingsSystem.getInstance(this);
		activityManager = new ActivityManagerSettings(this, settings);
		iabHelper = new IabHelper(this, getString(R.string.app_key_free));
		try {
			iabHelper.startSetup(controller);
		} catch(Exception exception) {
			errorMessage = exception.getLocalizedMessage();
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			iabHelper.dispose();
		} catch(Exception exception) {}
		iabHelper = null;
		activityManager.detachActivity(this);
	}
	
	@Override
	protected Dialog onCreateDialog(final int id) {
		DialogType dialogType;
		try {
			dialogType = DialogType.values()[id];
		} catch(IndexOutOfBoundsException exception) {
			return super.onCreateDialog(id);
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				removeDialog(id);
			}
		};
		
		switch(dialogType) {
			case DONATE_AGAIN:
				builder.setIcon(android.R.drawable.ic_dialog_info);
				builder.setTitle(R.string.confirm_donate_again_title);
				builder.setMessage(R.string.confirm_donate_again_message);
				listener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(which == Dialog.BUTTON_POSITIVE)
							performDonate();
						removeDialog(id);
					}
				};
				builder.setPositiveButton(R.string.yes, listener);
				builder.setNegativeButton(R.string.no, listener);
				return builder.create();
			case ERROR_IAB:
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(R.string.error);
				builder.setMessage(TextFormatter.getText(this, R.string.alert_donate_error_message, errorMessage));
				builder.setPositiveButton(R.string.ok, listener);
				return builder.create();
		}
		
		return super.onCreateDialog(id);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(iabHelper == null || !iabHelper.handleActivityResult(requestCode, resultCode, data))
			super.onActivityResult(requestCode, resultCode, data);
	}
	
	void performDonate() {
		donating = true;
		iabHelper.launchPurchaseFlow(this, DONATE_SKU, BUY_REQUEST_CODE, controller);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_settings, menu);
		inflater.inflate(R.menu.option_help, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.settings:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
			case R.id.help:
				(new HelpActivity.HelpOpener(this, getString(R.string.help_file))).openHelp();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
