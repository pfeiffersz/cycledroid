/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.tripslist;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.SettingsSystem;
import com.maral.cycledroid.asynctask.AsyncTaskQueue;
import com.maral.cycledroid.asynctask.AsyncTaskQueueImpl;
import com.maral.cycledroid.asynctask.AsyncTaskReceiver;
import com.maral.cycledroid.asynctask.ExtendedAsyncTask;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.DatabaseSQLite;
import com.maral.cycledroid.database.DeleteTripsTask;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.model.TripsList;

public final class MultiDeleteActivity extends MultiSelectActivity {
	private class Controller implements AsyncTaskReceiver {
		// TODO show checkboxes properly during deleting
		public void updateProgress(ExtendedAsyncTask task, int progress) {}
		
		public void taskStarts(ExtendedAsyncTask task) {
			if(task.getId() == taskId) {
				progressDialog = new ProgressDialog(MultiDeleteActivity.this);
				progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressDialog.setCancelable(false);
				progressDialog.setMessage(getString(R.string.progress_deleting_trips));
				progressDialog.show();
			}
		}
		
		public void taskFinishes(ExtendedAsyncTask task) {
			if(task.getId() == taskId) {
				removeProgressDialog();
				asyncTaskQueue.clearLast();
				finish();
			}
		}
	}
	
	private static enum SavedInstanceKey {
		TASK_ID,
	}
	
	private final Controller controller = new Controller();
	
	private static final int DIALOG_CONFIRM_DELETE = 2;
	
	private Database database;
	private ProgressDialog progressDialog = null;
	private AsyncTaskQueue asyncTaskQueue;
	private long taskId = ExtendedAsyncTask.NO_ID;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			getDoneButton().setText(R.string.delete);
		} catch(NullPointerException exception) {}
		Settings settings = SettingsSystem.getInstance(this);
		database = DatabaseSQLite.getInstance(this, settings);
		try {
			taskId = savedInstanceState.getLong(SavedInstanceKey.TASK_ID.name(), ExtendedAsyncTask.NO_ID);
		} catch(NullPointerException exception) {}
		asyncTaskQueue = AsyncTaskQueueImpl.getInstance();
		asyncTaskQueue.attach(controller);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putLong(SavedInstanceKey.TASK_ID.name(), taskId);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		asyncTaskQueue.detach(controller);
		database.finish(isFinishing());
		removeProgressDialog();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_delete_action, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.delete:
				selectDone();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected Dialog onCreateDialog(final int id) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		DialogInterface.OnClickListener listener;
		
		switch(id) {
			case DIALOG_CONFIRM_DELETE:
				listener = new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if(which == Dialog.BUTTON_POSITIVE) {
							long[] ids = getSelectedIds();
							List<Trip> trips = new ArrayList<Trip>();
							TripsList tripsList = database.getTripsList();
							for(long id : ids)
								trips.add(tripsList.getById(id));
							ExtendedAsyncTask task = new DeleteTripsTask(asyncTaskQueue, trips, database);
							taskId = task.getId();
							asyncTaskQueue.addTask(task);
						}
						removeDialog(id);
					}
				};
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(R.string.confirm_title_delete_trips);
				builder.setMessage(R.string.confirm_message_delete_trips);
				builder.setPositiveButton(R.string.yes, listener);
				builder.setNegativeButton(R.string.no, listener);
				return builder.create();
		}
		
		return super.onCreateDialog(id);
	}
	
	@Override
	protected void processSelection(long[] ids) {
		showDialog(DIALOG_CONFIRM_DELETE);
	}
	
	private void removeProgressDialog() {
		if(progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}
}
