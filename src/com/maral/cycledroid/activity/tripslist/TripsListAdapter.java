/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.tripslist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.maral.cycledroid.R;
import com.maral.cycledroid.formatter.PairFormatter;
import com.maral.cycledroid.formatter.ValuePair;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.model.TripsList;

class TripsListAdapter extends BaseAdapter {
	private final LayoutInflater inflater;
	private final TripsList tripsList;
	private final boolean withCheckbox;
	private final PairFormatter formatter;
	
	public TripsListAdapter(Context context, TripsList tripsList, PairFormatter formatter, boolean withCheckbox) {
		this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.tripsList = tripsList;
		this.formatter = formatter;
		this.withCheckbox = withCheckbox;
	}
	
	private static String format(ValuePair pair) {
		String unit = pair.getUnit();
		return pair.getValue() + (unit == null || unit.equals("")? "" : " " + unit);
	}
	
	public int getCount() {
		return tripsList.size();
	}
	
	public Trip getItem(int position) {
		return tripsList.get(position);
	}
	
	public long getItemId(int position) {
		return getItem(position).getId();
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null)
			convertView = inflater.inflate(R.layout.trip_item, null);
		convertView.findViewById(R.id.trip_item_checkbox).setVisibility(withCheckbox? View.VISIBLE : View.GONE);
		Trip trip = getItem(position);
		((TextView)convertView.findViewById(R.id.name)).setText(trip.getName());
		TextView descView = (TextView)convertView.findViewById(R.id.description);
		descView.setText(trip.getDescription());
		descView.setVisibility(trip.getDescription().compareTo("") == 0? View.GONE : View.VISIBLE);
		String speed = format(formatter.getAverageSpeed(trip));
		((TextView)convertView.findViewById(R.id.average_speed)).setText(speed);
		String distance = format(formatter.getDistance(trip));
		((TextView)convertView.findViewById(R.id.distance)).setText(distance);
		String time = format(formatter.getTime(trip));
		((TextView)convertView.findViewById(R.id.time)).setText(time);
		return convertView;
	}
}
