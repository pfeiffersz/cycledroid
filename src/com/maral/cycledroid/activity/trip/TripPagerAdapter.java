/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.trip;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.maral.cycledroid.R;
import com.maral.cycledroid.formatter.PairFormatter;
import com.maral.cycledroid.formatter.ValuePair;
import com.maral.cycledroid.model.HealthCalculator;
import com.maral.cycledroid.model.Trip;

class TripPagerAdapter extends PagerAdapter {
	private final LayoutInflater inflater;
	private final PairFormatter formatter;
	private final Trip trip;
	private final HealthCalculator healthCalculator;
	
	private final SparseArray<View> viewsMap = new SparseArray<View>();
	private final SparseArray<View> viewsToReuse = new SparseArray<View>();
	
	public TripPagerAdapter(Context context, PairFormatter formatter, Trip trip, HealthCalculator healthCalculator) {
		this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.formatter = formatter;
		this.trip = trip;
		this.healthCalculator = healthCalculator;
	}
	
	@Override
	public int getCount() {
		return 4;
	}
	
	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}
	
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View view = viewsToReuse.get(position);
		if(view != null)
			viewsToReuse.remove(position);
		else {
			int xml;
			switch(position) {
				case 0:
					xml = R.layout.trip_page0;
					break;
				case 1:
					xml = R.layout.trip_page1;
					break;
				case 2:
					xml = R.layout.trip_page2;
					break;
				case 3:
					xml = R.layout.trip_page3;
					break;
				default:
					throw new IllegalArgumentException("Incorrect position (" + position + ").");
					
			}
			view = inflater.inflate(xml, null);
		}
		updateView(view, position);
		container.addView(view);
		viewsMap.put(position, view);
		return view;
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View)object);
		viewsMap.remove(position);
		viewsToReuse.put(position, (View)object);
	}
	
	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		for(int i = 0; i < viewsMap.size(); ++i)
			updateView(viewsMap.valueAt(i), viewsMap.keyAt(i));
	}
	
	private void updateView(View view, int position) {
		switch(position) {
			case 0:
				updateCurrentSpeed(view);
				updateAverageSpeed(view);
				updateMaxSpeed(view);
				updateDistance(view);
				updateTripTime(view);
				updateAltitude(view);
				break;
			case 1:
				updateElevationAsc(view);
				updateElevationDesc(view);
				updateMinAltitude(view);
				updateMaxAltitude(view);
				updateInitialAltitude(view);
				updateFinalAltitude(view);
				break;
			case 2:
				updateBearing(view);
				updateSlope(view);
				updateTotalTime(view);
				updateBruttoSpeed(view);
				updatePaceNetto(view);
				updatePaceBrutto(view);
				break;
			case 3:
				updateStartTime(view);
				updateEndTime(view);
				updateTimeFromStart(view);
				updateCalories(view);
				updateFatBurned(view);
				updateOxygenConsumed(view);
				break;
			default:
				throw new IllegalArgumentException("Incorrect position (" + position + ").");
		}
	}
	
	private void updateValue(View view, ValuePair pair, int valueId, int unitId) {
		((TextView)view.findViewById(valueId)).setText(pair.getValue());
		((TextView)view.findViewById(unitId)).setText(pair.getUnit());
	}
	
	private void updateDateTimeValue(View view, ValuePair pair, int valueSingle, int valueLine1, int valueLine2) {
		String[] parts = pair.getValue().split("\n", 2);
		if(parts.length == 1) {
			((TextView)view.findViewById(valueSingle)).setText(parts[0]);
			view.findViewById(valueSingle).setVisibility(View.VISIBLE);
			view.findViewById(valueLine1).setVisibility(View.GONE);
			view.findViewById(valueLine2).setVisibility(View.GONE);
		} else {
			((TextView)view.findViewById(valueLine1)).setText(parts[0]);
			((TextView)view.findViewById(valueLine2)).setText(parts[1]);
			view.findViewById(valueSingle).setVisibility(View.GONE);
			view.findViewById(valueLine1).setVisibility(View.VISIBLE);
			view.findViewById(valueLine2).setVisibility(View.VISIBLE);
		}
	}
	
	private void updateCurrentSpeed(View view) {
		updateValue(view, formatter.getCurrentSpeed(trip), R.id.current_speed_value, R.id.current_speed_unit);
	}
	
	private void updateAverageSpeed(View view) {
		updateValue(view, formatter.getAverageSpeed(trip), R.id.average_speed_value, R.id.average_speed_unit);
	}
	
	private void updateMaxSpeed(View view) {
		updateValue(view, formatter.getMaxSpeed(trip), R.id.max_speed_value, R.id.max_speed_unit);
	}
	
	private void updateDistance(View view) {
		updateValue(view, formatter.getDistance(trip), R.id.distance_value, R.id.distance_unit);
	}
	
	private void updateTripTime(View view) {
		updateValue(view, formatter.getTime(trip), R.id.time_value, R.id.time_unit);
	}
	
	private void updateAltitude(View view) {
		updateValue(view, formatter.getAltitude(trip), R.id.altitude_value, R.id.altitude_unit);
	}
	
	private void updateElevationAsc(View view) {
		updateValue(view, formatter.getElevationAsc(trip), R.id.elevation_asc_value, R.id.elevation_asc_unit);
	}
	
	private void updateElevationDesc(View view) {
		updateValue(view, formatter.getElevationDesc(trip), R.id.elevation_desc_value, R.id.elevation_desc_unit);
	}
	
	private void updateMinAltitude(View view) {
		updateValue(view, formatter.getMinAltitude(trip), R.id.min_altitude_value, R.id.min_altitude_unit);
	}
	
	private void updateMaxAltitude(View view) {
		updateValue(view, formatter.getMaxAltitude(trip), R.id.max_altitude_value, R.id.max_altitude_unit);
	}
	
	private void updateTotalTime(View view) {
		updateValue(view, formatter.getTotalTime(trip), R.id.total_time_value, R.id.total_time_unit);
	}
	
	private void updateCalories(View view) {
		updateValue(view, formatter.getCalories(healthCalculator), R.id.calories_value, R.id.calories_unit);
	}
	
	private void updateFatBurned(View view) {
		updateValue(view, formatter.getFatBurned(healthCalculator), R.id.fat_burned_value, R.id.fat_burned_unit);
	}
	
	private void updateOxygenConsumed(View view) {
		updateValue(view, formatter.getOxygenConsumed(healthCalculator), R.id.oxygen_consumed_value,
				R.id.oxygen_consumed_unit);
	}
	
	private void updatePaceNetto(View view) {
		updateValue(view, formatter.getPaceNetto(trip), R.id.pace_netto_value, R.id.pace_netto_unit);
	}
	
	private void updatePaceBrutto(View view) {
		updateValue(view, formatter.getPaceBrutto(trip), R.id.pace_brutto_value, R.id.pace_brutto_unit);
	}
	
	private void updateBearing(View view) {
		updateValue(view, formatter.getBearing(trip), R.id.bearing_value, R.id.bearing_unit);
	}
	
	private void updateBruttoSpeed(View view) {
		updateValue(view, formatter.getSpeedBrutto(trip), R.id.brutto_speed_value, R.id.brutto_speed_unit);
	}
	
	private void updateSlope(View view) {
		updateValue(view, formatter.getSlope(trip), R.id.slope_value, R.id.slope_unit);
	}
	
	private void updateTimeFromStart(View view) {
		updateValue(view, formatter.getTimeFromStart(trip), R.id.time_from_start_value, R.id.time_from_start_unit);
	}
	
	private void updateInitialAltitude(View view) {
		updateValue(view, formatter.getInitialAltitude(trip), R.id.initial_altitude_value, R.id.initial_altitude_unit);
	}
	
	private void updateFinalAltitude(View view) {
		updateValue(view, formatter.getFinalAltitude(trip), R.id.final_altitude_value, R.id.final_altitude_unit);
	}
	
	private void updateStartTime(View view) {
		updateDateTimeValue(view, formatter.getStartTime(trip), R.id.start_time_single, R.id.start_time_line1,
				R.id.start_time_line2);
	}
	
	private void updateEndTime(View view) {
		updateDateTimeValue(view, formatter.getEndTime(trip), R.id.end_time_single, R.id.end_time_line1,
				R.id.end_time_line2);
	}
}
