/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.trip;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.ClipboardManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.maral.cycledroid.AppInfo;
import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.DonateActivity;
import com.maral.cycledroid.activity.FormatterSettings;
import com.maral.cycledroid.activity.HealthCalculatorSettings;
import com.maral.cycledroid.activity.HelpActivity;
import com.maral.cycledroid.activity.TextFormatter;
import com.maral.cycledroid.activity.TripEditActivity;
import com.maral.cycledroid.activity.facebook.FacebookActivity;
import com.maral.cycledroid.activity.file.ChooseFileActivity;
import com.maral.cycledroid.activity.graph.GraphActivity;
import com.maral.cycledroid.activity.map.MapActivity;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.SettingsActivity;
import com.maral.cycledroid.activity.settings.SettingsSystem;
import com.maral.cycledroid.asynctask.AsyncTaskQueue;
import com.maral.cycledroid.asynctask.AsyncTaskQueueImpl;
import com.maral.cycledroid.asynctask.AsyncTaskReceiver;
import com.maral.cycledroid.asynctask.ExtendedAsyncTask;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.DatabaseSQLite;
import com.maral.cycledroid.exporter.CSVExporter;
import com.maral.cycledroid.exporter.ExportTripTask;
import com.maral.cycledroid.exporter.Exporter;
import com.maral.cycledroid.exporter.GPXExporter;
import com.maral.cycledroid.exporter.KMLExporter;
import com.maral.cycledroid.formatter.PairFormatter;
import com.maral.cycledroid.formatter.ValuePair;
import com.maral.cycledroid.model.HealthCalculator;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.model.TripMulti;
import com.maral.cycledroid.service.ServiceState;
import com.maral.cycledroid.service.ServiceState.State;
import com.maral.cycledroid.service.TripService;
import com.maral.cycledroid.ui.ActivityManager;
import com.maral.cycledroid.ui.ActivityManagerSettings;
import com.maral.cycledroid.widget.numberpicker.NumberPicker;
import com.maral.cycledroid.widget.pager.CyclicPagerAdapter;

@SuppressWarnings("deprecation")
public final class TripActivity extends Activity {
	private class Controller implements OnClickListener, Observer, OnTouchListener, OnPageChangeListener,
			AsyncTaskReceiver {
		public void onClick(View view) {
			switch(view.getId()) {
				case R.id.play:
					controlService(true);
					showPauseButton();
					break;
				case R.id.pause:
					controlService(false);
					showPlayButton();
					break;
				case R.id.graphs:
					showDialog(DialogType.CHOOSE_GRAPH.ordinal());
					break;
			}
		}
		
		public void update(final Observable observable, final Object data) {
			Runnable run = new Runnable() {
				public void run() {
					if(observable == TripService.NOTIFIER)
						onServiceStateChanged((ServiceState)data);
					else if(observable == healthCalculator || observable == trip || observable == formatter)
						onTripChanged();
				}
				
			};
			runOnUiThread(run);
		}
		
		private void onServiceStateChanged(ServiceState state) {
			switch(state.getState()) {
				case FIX:
					statusView.setText(statusFix);
					showPauseButton();
					break;
				case NO_FIX:
					statusView.setText(statusNoFix);
					showPauseButton();
					break;
				case ACQUIRING:
					statusView.setText(statusAcquiring);
					showPauseButton();
					break;
				case GPS_DISABLED:
					statusView.setText(statusGpsDisabled);
					Toast.makeText(TripActivity.this, R.string.toast_gps_disabled, Toast.LENGTH_LONG).show();
					showPauseButton();
					break;
				case SERVICE_DISABLED:
					statusView.setText(statusServiceDisabled);
					showPlayButton();
					break;
			}
			satellitesView.setText(Integer.toString(state.getSatellitesCount()));
			// for screenshots
			//satellitesView.setText("14"); statusView.setText(statusFix);
		}
		
		private void onTripChanged() {
			updateAll();
		}
		
		@SuppressLint("ClickableViewAccessibility")
		public boolean onTouch(View view, MotionEvent event) {
			switch(view.getId()) {
				case R.id.pager:
					if(autoScrollDelay > 0) {
						handler.removeCallbacks(AUTO_SCROLL);
						autoScrollDelay = 0;
						Toast.makeText(TripActivity.this, R.string.toast_auto_scroll_stop, Toast.LENGTH_SHORT).show();
					}
					break;
			}
			return false;
		}
		
		public void onPageScrollStateChanged(int state) {}
		
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
		
		public void onPageSelected(int position) {
			if(!movedScreen) {
				appInfo.setMovedScreen(true);
				movedScreen = true;
			}
		}
		
		public void taskFinishes(ExtendedAsyncTask task) {
			if(task.getId() == taskId) {
				removeProgressDialog();
				ExportTripTask exportTripTask = (ExportTripTask)task; // TODO this sometimes generates ClassCastException
				String exportPath = exportTripTask.getExportPath();
				CharSequence toastText;
				if(exportTripTask.wasExported())
					toastText = TextFormatter.getText(TripActivity.this, R.string.toast_saved_file, exportPath);
				else
					toastText = TextFormatter.getText(TripActivity.this, R.string.toast_export_failed, exportPath);
				Toast.makeText(TripActivity.this, toastText, Toast.LENGTH_LONG).show();
				asyncTaskQueue.clearLast();
			}
		}
		
		public void taskStarts(ExtendedAsyncTask task) {
			if(task.getId() == taskId) {
				progressDialog = new ProgressDialog(TripActivity.this);
				progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				progressDialog.setMax(100);
				progressDialog.setCancelable(false);
				progressDialog.setMessage(getString(R.string.progress_exporting));
				progressDialog.show();
			}
		}
		
		public void updateProgress(ExtendedAsyncTask task, int progress) {
			if(task.getId() == taskId)
				if(progressDialog != null)
					progressDialog.setProgress(progress);
		}
	}
	
	private Controller controller = new Controller();
	
	private static enum ExportType {
		CSV, GPX, KML,
	}
	
	private static enum DialogType {
		CONFIRM_EXIT, CHOOSE_GRAPH, EXPORT, AUTO_CHANGE, FACEBOOK_EMPTY, FACEBOOK_UNSUPPORTED,
	}
	
	private static enum RequestType {
		EXPORT_CSV, EXPORT_GPX, EXPORT_KML,
	}
	
	private static enum SavedInstanceKey {
		AUTO_SCROLL, TASK_ID,
	}
	
	private static enum IntentKey {
		TRIP_ID, TRIPS_ARRAY,
	}
	
	// id of a trip to open
	public static final String INTENT_TRIP_ID = IntentKey.TRIP_ID.name();
	// list of ids for summary
	public static final String INTENT_TRIPS_ARRAY = IntentKey.TRIPS_ARRAY.name();
	
	private static final int MIN_AUTO_SCROLL = 1;
	private static final int MAX_AUTO_SCROLL = 60 * 60;
	
	private static final long NO_TRIP = -1;
	
	private Settings settings;
	private Database database;
	private PairFormatter formatter;
	private AppInfo appInfo;
	private ActivityManager activityManager;
	
	private Trip trip;
	private HealthCalculator healthCalculator = null;
	
	private String graphDistanceAltitude;
	private String graphDistanceSpeed;
	private String graphTimeSpeed;
	private String[] graphs;
	
	private TextView statusView;
	private TextView satellitesView;
	private ViewPager viewPager;
	private CyclicPagerAdapter adapter;
	private boolean movedScreen = false;
	private long taskId = ExtendedAsyncTask.NO_ID;
	
	private String statusFix;
	private String statusNoFix;
	private String statusAcquiring;
	private String statusGpsDisabled;
	private String statusServiceDisabled;
	
	private ProgressDialog progressDialog = null;
	private AsyncTaskQueue asyncTaskQueue = null;
	
	private long autoScrollDelay = 0;
	private Handler handler = new Handler();
	
	private final Runnable AUTO_SCROLL = new Runnable() {
		public void run() {
			viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, false);
			handler.postDelayed(this, autoScrollDelay);
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trip);
		
		settings = SettingsSystem.getInstance(this);
		database = DatabaseSQLite.getInstance(this, settings);
		formatter = new FormatterSettings(this, settings);
		activityManager = new ActivityManagerSettings(this, settings);
		appInfo = new AppInfo(this);
		
		if(getIntent().hasExtra(INTENT_TRIP_ID)) {
			long tripId = getIntent().getLongExtra(INTENT_TRIP_ID, NO_TRIP);
			trip = database.getTripsList().getById(tripId);
		} else if(getIntent().hasExtra(INTENT_TRIPS_ARRAY)) {
			long[] ids = getIntent().getLongArrayExtra(INTENT_TRIPS_ARRAY);
			List<Trip> trips = new ArrayList<Trip>();
			for(long id : ids)
				trips.add(database.getTripsList().getById(id));
			trip = new TripMulti(getString(R.string.trips_summary_name), trips);
		}
		if(trip == null) { // this should never happen, but error reports show that sometimes happens...
			finish();
			return;
		}
		healthCalculator = new HealthCalculatorSettings(trip, settings);
		
		if(trip.providesTracking()) {
			findViewById(R.id.play).setOnClickListener(controller);
			findViewById(R.id.pause).setOnClickListener(controller);
		} else
			((ImageButton)findViewById(R.id.play)).setImageResource(R.drawable.play_inactive);
		
		if(trip.providesGraphs())
			findViewById(R.id.graphs).setOnClickListener(controller);
		else
			((ImageButton)findViewById(R.id.graphs)).setImageResource(R.drawable.graphs_inactive);
		
		statusView = (TextView)findViewById(R.id.status);
		satellitesView = (TextView)findViewById(R.id.satellites_count);
		viewPager = (ViewPager)findViewById(R.id.pager);
		adapter = new CyclicPagerAdapter(new TripPagerAdapter(this, formatter, trip, healthCalculator));
		viewPager.setAdapter(adapter);
		viewPager.setOnPageChangeListener(controller);
		updateAll();
		
		graphDistanceAltitude = getString(R.string.distance_altitude);
		graphDistanceSpeed = getString(R.string.distance_speed);
		graphTimeSpeed = getString(R.string.time_speed);
		graphs = new String[] {
			graphDistanceAltitude, graphDistanceSpeed, graphTimeSpeed
		};
		Arrays.sort(graphs);
		
		statusFix = getString(R.string.status_fix);
		statusNoFix = getString(R.string.status_no_fix);
		statusAcquiring = getString(R.string.status_acquiring);
		statusGpsDisabled = getString(R.string.status_gps_disabled);
		statusServiceDisabled = getString(R.string.status_service_disabled);
		
		trip.addObserver(controller);
		healthCalculator.addObserver(controller);
		formatter.addObserver(controller);
		TripService.NOTIFIER.addObserver(controller);
		
		try {
			taskId = savedInstanceState.getLong(SavedInstanceKey.TASK_ID.name(), ExtendedAsyncTask.NO_ID);
		} catch(NullPointerException exception) {}
		asyncTaskQueue = AsyncTaskQueueImpl.getInstance();
		asyncTaskQueue.attach(controller);
		
		movedScreen = new AppInfo(this).getMovedScreen();
		// "savedInstanceState == null" means that activity is not being recreated
		if(!movedScreen && savedInstanceState == null)
			Toast.makeText(this, R.string.toast_move_screen, Toast.LENGTH_LONG).show();
		viewPager.setOnTouchListener(controller);
		
		if(savedInstanceState == null) {
			if(trip.providesTracking() && settings.getAutoStartTracking()
					&& TripService.getState().getState() == State.SERVICE_DISABLED) {
				controlService(true);
				Toast.makeText(this, R.string.toast_auto_start_tracking, Toast.LENGTH_LONG).show();
			}
		} else {
			long delay = savedInstanceState.getLong(SavedInstanceKey.AUTO_SCROLL.name(), 0);
			if(delay > 0)
				autoChangeStart(delay, false);
		}
		
		controller.onServiceStateChanged(TripService.getState());
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putLong(SavedInstanceKey.AUTO_SCROLL.name(), autoScrollDelay);
		outState.putLong(SavedInstanceKey.TASK_ID.name(), taskId);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		handler.removeCallbacks(AUTO_SCROLL);
		removeProgressDialog(); // needed because of an error "activity has leaked window"
		activityManager.detachActivity(this);
		try {
			trip.deleteObserver(controller);
			if(isFinishing() && trip.providesTracking())
				trip.stop();
			healthCalculator.deleteObserver(controller);
			asyncTaskQueue.detach(controller);
		} catch(NullPointerException exception) {} // in case of unexplained error in onCreate
		formatter.deleteObserver(controller);
		TripService.NOTIFIER.deleteObserver(controller);
		database.finish(isFinishing());
		
	}
	
	private void updateAll() {
		setTitle(trip.getName());
		adapter.notifyDataSetChanged();
	}
	
	private void controlService(boolean start) {
		if(start) {
			Intent intent = new Intent(this, TripService.class);
			intent.putExtra(TripService.INTENT_TRIP_ID, trip.getId());
			startService(intent);
		} else
			stopService(new Intent(this, TripService.class));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		if(trip.providesEdit())
			inflater.inflate(R.menu.option_edit, menu);
		if(trip.providesExport())
			inflater.inflate(R.menu.option_export, menu);
		if(trip.providesMap())
			inflater.inflate(R.menu.option_map, menu);
		if(trip.providesShare())
			inflater.inflate(R.menu.option_share, menu);
		inflater.inflate(R.menu.option_scroll, menu);
		inflater.inflate(R.menu.option_copy, menu);
		inflater.inflate(R.menu.option_settings, menu);
		inflater.inflate(R.menu.option_help, menu);
		inflater.inflate(R.menu.option_donate, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.donate).setVisible(!settings.getRemoveDonate());
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch(item.getItemId()) {
			case R.id.edit:
				intent = new Intent(this, TripEditActivity.class);
				intent.putExtra(TripEditActivity.INTENT_NEW, false);
				intent.putExtra(TripEditActivity.INTENT_TRIP_ID, trip.getId());
				startActivity(intent);
				return true;
			case R.id.export:
				showDialog(DialogType.EXPORT.ordinal());
				return true;
			case R.id.map:
				intent = new Intent(this, MapActivity.class);
				intent.putExtra(MapActivity.INTENT_TRIP_ID, trip.getId());
				startActivity(intent);
				return true;
			case R.id.share:
				if(trip.getStartTime() == null)
					showDialog(DialogType.FACEBOOK_EMPTY.ordinal());
				else if(!FacebookActivity.canShare(this))
					showDialog(DialogType.FACEBOOK_UNSUPPORTED.ordinal());
				else {
					intent = new Intent(this, FacebookActivity.class);
					intent.putExtra(FacebookActivity.INTENT_TRIP_ID, trip.getId());
					startActivity(intent);
				}
				return true;
			case R.id.scroll:
				showDialog(DialogType.AUTO_CHANGE.ordinal());
				return true;
			case R.id.copy_to_clipboard:
				copyToClipboard();
				return true;
			case R.id.settings:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
			case R.id.help:
				(new HelpActivity.HelpOpener(this, getString(R.string.help_file))).openHelp();
				return true;
			case R.id.donate:
				startActivity(new Intent(this, DonateActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	private void addNewValueToText(StringBuilder builder, int name, ValuePair pair) {
		builder.append(getString(name));
		builder.append(": ");
		builder.append(pair.getValue().replace('\n', ' '));
		if(pair.getUnit() != null)
			builder.append(" " + pair.getUnit());
		builder.append("\r\n");
	}
	
	private void copyToClipboard() {
		StringBuilder builder = new StringBuilder();
		builder.append(trip.getName());
		builder.append("\r\n");
		addNewValueToText(builder, R.string.current_speed, formatter.getCurrentSpeed(trip));
		addNewValueToText(builder, R.string.average_speed, formatter.getAverageSpeed(trip));
		addNewValueToText(builder, R.string.max_speed, formatter.getMaxSpeed(trip));
		addNewValueToText(builder, R.string.distance, formatter.getDistance(trip));
		addNewValueToText(builder, R.string.time, formatter.getTime(trip));
		addNewValueToText(builder, R.string.altitude, formatter.getAltitude(trip));
		addNewValueToText(builder, R.string.elevation_asc, formatter.getElevationAsc(trip));
		addNewValueToText(builder, R.string.elevation_desc, formatter.getElevationDesc(trip));
		addNewValueToText(builder, R.string.min_altitude, formatter.getMinAltitude(trip));
		addNewValueToText(builder, R.string.max_altitude, formatter.getMaxAltitude(trip));
		addNewValueToText(builder, R.string.initial_altitude, formatter.getInitialAltitude(trip));
		addNewValueToText(builder, R.string.final_altitude, formatter.getFinalAltitude(trip));
		addNewValueToText(builder, R.string.bearing, formatter.getBearing(trip));
		addNewValueToText(builder, R.string.slope, formatter.getSlope(trip));
		addNewValueToText(builder, R.string.total_time, formatter.getTotalTime(trip));
		addNewValueToText(builder, R.string.brutto_speed, formatter.getSpeedBrutto(trip));
		addNewValueToText(builder, R.string.pace_netto, formatter.getPaceNetto(trip));
		addNewValueToText(builder, R.string.pace_brutto, formatter.getPaceBrutto(trip));
		addNewValueToText(builder, R.string.start_time, formatter.getStartTime(trip));
		addNewValueToText(builder, R.string.end_time, formatter.getEndTime(trip));
		addNewValueToText(builder, R.string.time_from_start, formatter.getTimeFromStart(trip));
		addNewValueToText(builder, R.string.calories, formatter.getCalories(healthCalculator));
		addNewValueToText(builder, R.string.fat_burned, formatter.getFatBurned(healthCalculator));
		addNewValueToText(builder, R.string.oxygen_consumed, formatter.getOxygenConsumed(healthCalculator));
		// TODO do something with deprecated ClipboardManager
		ClipboardManager clipboard = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
		clipboard.setText(builder.toString());
		Toast.makeText(this, R.string.copied_to_clipboard, Toast.LENGTH_SHORT).show();
	}
	
	@Override
	protected Dialog onCreateDialog(final int id) {
		DialogType dialogType;
		try {
			dialogType = DialogType.values()[id];
		} catch(IndexOutOfBoundsException exception) {
			return super.onCreateDialog(id);
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				removeDialog(id);
			}
		};
		
		switch(dialogType) {
			case CONFIRM_EXIT:
				listener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(which == Dialog.BUTTON_POSITIVE) {
							if(TripService.getState().getState() != State.SERVICE_DISABLED)
								controlService(false);
							finish();
						}
						removeDialog(id);
					}
				};
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(R.string.confirm_title_close_trip);
				builder.setMessage(R.string.confirm_message_close_trip);
				builder.setPositiveButton(R.string.yes, listener);
				builder.setNegativeButton(R.string.no, listener);
				return builder.create();
			case CHOOSE_GRAPH:
				listener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {
						Intent intent = new Intent(TripActivity.this, GraphActivity.class);
						intent.putExtra(GraphActivity.INTENT_TRIP_ID, trip.getId());
						if(graphs[item].compareTo(graphDistanceAltitude) == 0) {
							intent.putExtra(GraphActivity.INTENT_TYPE, GraphActivity.TYPE_DISTANCE_ALTITUDE);
							startActivity(intent);
						} else if(graphs[item].compareTo(graphDistanceSpeed) == 0) {
							intent.putExtra(GraphActivity.INTENT_TYPE, GraphActivity.TYPE_DISTANCE_SPEED);
							startActivity(intent);
						} else if(graphs[item].compareTo(graphTimeSpeed) == 0) {
							intent.putExtra(GraphActivity.INTENT_TYPE, GraphActivity.TYPE_TIME_SPEED);
							startActivity(intent);
						}
						removeDialog(id);
					}
				};
				builder.setTitle(R.string.list_select_type);
				builder.setItems(graphs, listener);
				return builder.create();
			case EXPORT:
				final String[] types = new String[ExportType.values().length];
				types[ExportType.CSV.ordinal()] = getString(R.string.csv);
				types[ExportType.GPX.ordinal()] = getString(R.string.gpx);
				types[ExportType.KML.ordinal()] = getString(R.string.kml);
				listener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {
						export(ExportType.values()[item]);
						removeDialog(id);
					}
				};
				builder.setTitle(R.string.dialog_export_format);
				builder.setItems(types, listener);
				return builder.create();
			case AUTO_CHANGE:
				LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View view = inflater.inflate(R.layout.auto_change_dialog, null);
				final NumberPicker numberPicker = (NumberPicker)view.findViewById(R.id.auto_change_value);
				numberPicker.setRange(MIN_AUTO_SCROLL, MAX_AUTO_SCROLL);
				numberPicker.setCurrent((new AppInfo(this)).getLastInterval());
				listener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(which == Dialog.BUTTON_POSITIVE) {
							int interval = numberPicker.getCurrent();
							autoChangeStart(interval * 1000, true);
							(new AppInfo(TripActivity.this)).setLastInterval(interval);
						}
						removeDialog(id);
					}
				};
				builder.setMessage(R.string.dialog_auto_change_message);
				// no title - doesn't fit on a screen
				builder.setPositiveButton(R.string.ok, listener);
				builder.setNegativeButton(R.string.cancel, listener);
				builder.setView(view);
				return builder.create();
			case FACEBOOK_EMPTY:
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(R.string.error);
				builder.setMessage(R.string.alert_message_facebook_empty);
				builder.setNeutralButton(R.string.ok, listener);
				return builder.create();
			case FACEBOOK_UNSUPPORTED:
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(R.string.error);
				builder.setMessage(R.string.alert_message_facebook_unsupported);
				builder.setNeutralButton(R.string.ok, listener);
				return builder.create();
		}
		
		return super.onCreateDialog(id);
	}
	
	private void autoChangeStart(long interval, boolean toast) {
		handler.removeCallbacks(AUTO_SCROLL);
		autoScrollDelay = interval;
		handler.postDelayed(AUTO_SCROLL, autoScrollDelay);
		if(toast) {
			int seconds = (int)(interval / 1000);
			String text = getResources().getQuantityString(R.plurals.toast_auto_scroll_start, seconds, seconds);
			Toast.makeText(this, text, Toast.LENGTH_LONG).show();
		}
	}
	
	private void export(ExportType type) {
		Intent intent = (new Intent(this, ChooseFileActivity.class));
		intent.putExtra(ChooseFileActivity.INTENT_MODE, ChooseFileActivity.MODE_SAVE_SINGLE);
		String startPath = appInfo.getLastExportPath();
		if(startPath == AppInfo.NO_IMPORT_PATH || !(new File(startPath).exists()))
			startPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		intent.putExtra(ChooseFileActivity.INTENT_START_PATH, startPath);
		intent.putExtra(ChooseFileActivity.INTENT_FILENAME_PREFIX, trip.getName());
		String extension = null;
		RequestType requestType = null;
		switch(type) {
			case CSV:
				extension = ".csv";
				requestType = RequestType.EXPORT_CSV;
				break;
			case GPX:
				extension = ".gpx";
				requestType = RequestType.EXPORT_GPX;
				break;
			case KML:
				extension = ".kml";
				requestType = RequestType.EXPORT_KML;
				break;
		}
		intent.putExtra(ChooseFileActivity.INTENT_FILENAME_SUFFIX, extension);
		startActivityForResult(intent, requestType.ordinal());
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == Activity.RESULT_OK) {
			RequestType requestType;
			try {
				requestType = RequestType.values()[requestCode];
			} catch(IndexOutOfBoundsException exception) {
				return;
			}
			
			String exportPath = data.getStringExtra(ChooseFileActivity.RESULT_PATH);
			appInfo.setLastExportPath((new File(exportPath)).getParent());
			Exporter exporter = null;
			switch(requestType) {
				case EXPORT_CSV:
					exporter = new CSVExporter(database);
					break;
				case EXPORT_GPX:
					exporter = new GPXExporter(this, database);
					break;
				case EXPORT_KML:
					exporter = new KMLExporter(database);
					break;
			}
			
			ExtendedAsyncTask task = new ExportTripTask(asyncTaskQueue, exportPath, exporter, trip);
			taskId = task.getId();
			asyncTaskQueue.addTask(task);
		}
	}
	
	private void showPauseButton() {
		((ImageButton)findViewById(R.id.play)).setVisibility(View.GONE);
		((ImageButton)findViewById(R.id.pause)).setVisibility(View.VISIBLE);
	}
	
	private void showPlayButton() {
		((ImageButton)findViewById(R.id.play)).setVisibility(View.VISIBLE);
		((ImageButton)findViewById(R.id.pause)).setVisibility(View.GONE);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK && TripService.getState().getState() != State.SERVICE_DISABLED)
			showDialog(DialogType.CONFIRM_EXIT.ordinal());
		return super.onKeyDown(keyCode, event);
	}
	
	private void removeProgressDialog() {
		if(progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}
}
