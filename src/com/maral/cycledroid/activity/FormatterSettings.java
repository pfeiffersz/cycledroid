/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity;

import java.util.Observable;
import java.util.Observer;

import android.content.Context;

import com.maral.cycledroid.WeakObserver;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.formatter.PairFormatter;
import com.maral.cycledroid.model.Unit.Altitude;
import com.maral.cycledroid.model.Unit.Angle;
import com.maral.cycledroid.model.Unit.Distance;
import com.maral.cycledroid.model.Unit.Duration;
import com.maral.cycledroid.model.Unit.Energy;
import com.maral.cycledroid.model.Unit.Pace;
import com.maral.cycledroid.model.Unit.Power;
import com.maral.cycledroid.model.Unit.Speed;
import com.maral.cycledroid.model.Unit.Volume;
import com.maral.cycledroid.model.Unit.Weight;

public class FormatterSettings extends PairFormatter implements Observer {
	private final Settings settings;
	
	public FormatterSettings(Context context, Settings settings) {
		super(context);
		this.settings = settings;
		settings.addObserver(new WeakObserver(this));
		
		setSettingsUnits();
		// set also units that are not changed by settings
		super.setBearingUnit(Angle.DEGREE);
		super.setDurationUnit(Duration.S);
		super.setEnergyUnit(Energy.KCAL);
		super.setPowerUnit(Power.W);
		super.setSlopeUnit(Angle.PERCENT);
	}
	
	public void update(Observable observable, Object data) {
		// for simplicity we don't check which setting has been changed and update all the units
		if(observable == settings)
			setSettingsUnits();
	}
	
	private void setSettingsUnits() {
		super.setAltitudeUnit(settings.getAltitudeUnit());
		super.setDistanceUnit(settings.getDistanceUnit());
		super.setPaceUnit(settings.getPaceUnit());
		super.setSpeedUnit(settings.getSpeedUnit());
		super.setVolumeUnit(settings.getVolumeUnit());
		super.setWeightUnit(settings.getWeightUnit());
	}
	
	private void throwSettingsException() {
		throw new UnsupportedOperationException("This parameter is set automatically by settings.");
	}
	
	@Override
	public void setAltitudeUnit(Altitude unit) {
		throwSettingsException();
	}
	
	@Override
	public void setDistanceUnit(Distance unit) {
		throwSettingsException();
	}
	
	@Override
	public void setPaceUnit(Pace unit) {
		throwSettingsException();
	}
	
	@Override
	public void setSpeedUnit(Speed unit) {
		throwSettingsException();
	}
	
	@Override
	public void setVolumeUnit(Volume unit) {
		throwSettingsException();
	}
	
	@Override
	public void setWeightUnit(Weight unit) {
		throwSettingsException();
	}
}
