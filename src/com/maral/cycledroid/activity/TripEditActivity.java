/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity;

import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.SettingsActivity;
import com.maral.cycledroid.activity.settings.SettingsSystem;
import com.maral.cycledroid.activity.trip.TripActivity;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.DatabaseSQLite;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.ui.ActivityManager;
import com.maral.cycledroid.ui.ActivityManagerSettings;

public final class TripEditActivity extends Activity {
	private final class Controller implements OnClickListener {
		public void onClick(View view) {
			switch(view.getId()) {
				case R.id.save:
					buttonSave();
					break;
				case R.id.cancel:
					buttonCancel();
					break;
			}
		}
	}
	
	private final Controller controller = new Controller();
	
	private static enum DialogType {
		PROVIDE_NAME, CONFIRM_EXIT,
	}
	
	private static enum IntentKey {
		NEW, TRIP_ID,
	}
	
	// whether creating a new trip or editing an existing one
	public static final String INTENT_NEW = IntentKey.NEW.name();
	// id of a trip to edit, if INTENT_NEW is false
	public static final String INTENT_TRIP_ID = IntentKey.TRIP_ID.name();
	
	private Settings settings;
	private Database database;
	private ActivityManager activityManager;
	
	private Trip trip = null;
	private String name;
	private String description;
	private String previousName;
	private String previousDescription;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		settings = SettingsSystem.getInstance(this);
		database = DatabaseSQLite.getInstance(this, settings);
		activityManager = new ActivityManagerSettings(this, settings);
		
		setContentView(R.layout.trip_edit);
		// The buttons "Save" and "Cancel" are not visible on newer devices.
		try {
			findViewById(R.id.save).setOnClickListener(controller);
			findViewById(R.id.cancel).setOnClickListener(controller);
		} catch(NullPointerException exception) {}
		
		EditText nameField = ((EditText)findViewById(R.id.name));
		EditText descriptionField = ((EditText)findViewById(R.id.description));
		if(!getIntent().getBooleanExtra(INTENT_NEW, true)) {
			long tripId = getIntent().getLongExtra(INTENT_TRIP_ID, 0);
			trip = database.getTripsList().getById(tripId);
			((TextView)findViewById(R.id.name)).append(trip.getName());
			descriptionField.append(trip.getDescription());
			setTitle(trip.getName());
		} else {
			// provide the default name (containing a date)
			setTitle(R.string.label_create_trip);
			nameField.append(getString(R.string.new_trip, DateFormat.getDateFormat(this).format(new Date())));
			nameField.selectAll();
		}
		
		previousName = nameField.getText().toString();
		previousDescription = descriptionField.getText().toString();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		activityManager.detachActivity(this);
		database.finish(isFinishing());
	}
	
	@Override
	protected Dialog onCreateDialog(final int id) {
		DialogType dialogType;
		try {
			dialogType = DialogType.values()[id];
		} catch(IndexOutOfBoundsException excetpion) {
			return super.onCreateDialog(id);
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				removeDialog(id);
			}
		};
		
		switch(dialogType) {
			case PROVIDE_NAME:
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(R.string.error);
				builder.setMessage(R.string.alert_message_enter_name);
				builder.setNeutralButton(R.string.ok, listener);
				return builder.create();
			case CONFIRM_EXIT:
				listener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(which == Dialog.BUTTON_POSITIVE)
							finish();
						removeDialog(id);
					}
				};
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(R.string.confirm_title_edit_close);
				builder.setMessage(R.string.confirm_message_edit_close);
				builder.setPositiveButton(R.string.yes, listener);
				builder.setNegativeButton(R.string.no, listener);
				return builder.create();
		}
		
		return super.onCreateDialog(id);
	}
	
	private void readValues() {
		name = ((EditText)findViewById(R.id.name)).getText().toString().trim();
		description = ((EditText)findViewById(R.id.description)).getText().toString().trim();
	}
	
	private void buttonSave() {
		readValues();
		if(name.compareTo("") == 0)
			showDialog(DialogType.PROVIDE_NAME.ordinal());
		else {
			if(trip == null) {
				trip = database.createTripAndAdd(name, description);
				// open trip immediatly after adding
				Intent intent = new Intent(this, TripActivity.class);
				intent.putExtra(TripActivity.INTENT_TRIP_ID, trip.getId());
				startActivity(intent);
			} else
				database.updateTrip(trip, name, description);
			finish();
		}
	}
	
	private void buttonCancel() {
		// warn user if he changed anything
		readValues();
		boolean empty = name.compareTo("") == 0 && description.compareTo("") == 0;
		if(empty || (name.compareTo(previousName) == 0 && description.compareTo(previousDescription) == 0))
			finish();
		else
			showDialog(DialogType.CONFIRM_EXIT.ordinal());
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_save, menu);
		inflater.inflate(R.menu.option_cancel, menu);
		inflater.inflate(R.menu.option_settings, menu);
		inflater.inflate(R.menu.option_help, menu);
		inflater.inflate(R.menu.option_donate, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.donate).setVisible(!settings.getRemoveDonate());
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.save:
				buttonSave();
				return true;
			case R.id.cancel:
				buttonCancel();
				return true;
			case R.id.settings:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
			case R.id.help:
				(new HelpActivity.HelpOpener(this, getString(R.string.help_file))).openHelp();
				return true;
			case R.id.donate:
				startActivity(new Intent(this, DonateActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			buttonCancel();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
