/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.mock;

import android.content.Context;

import com.maral.cycledroid.R;

public class ExampleTripWeekend extends ExampleTrip {
	public ExampleTripWeekend(Context context) {
		super(context, R.string.example_weekend_name, R.string.example_weekend_description);
	}
	
	@Override
	public Long getId() {
		return -5L;
	}
	
	@Override
	public float getDistance() {
		return 188049.33f;
	}
	
	@Override
	public float getTime() {
		return 38915000f;
	}
	
	@Override
	public Float getCurrentSpeed() {
		return 7.15f;
	}
	
	@Override
	public Float getMaxSpeed() {
		return 10.56f;
	}
	
	@Override
	public Float getAltitude() {
		return 201.41f;
	}
	
	@Override
	public float getElevationAsc() {
		return 738.56f;
	}
	
	@Override
	public float getElevationDesc() {
		return 842.09f;
	}
	
	@Override
	public Float getMinAltitude() {
		return 178.40f;
	}
	
	@Override
	public Float getMaxAltitude() {
		return 571.29f;
	}
	
	@Override
	public float getTotalTime() {
		return 43569000f;
	}
	
	@Override
	public Float getBearing() {
		return 192.05f;
	}
	
	@Override
	public Float getSlope() {
		return 12.04f;
	}
	
	@Override
	public Float getPowerFactor() {
		return 23.18f;
	}
	
	@Override
	public Float getInitialAltitude() {
		return 444.15f;
	}
	
	@Override
	public Long getStartTime() {
		return dateFromString("22.06.2013 11:54:31");
	}
	
	@Override
	public Long getEndTime() {
		return dateFromString("23.06.2013 18:26:09");
	}
}
