/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.facebook;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONObject;

import com.facebook.model.GraphObject;
import com.facebook.model.GraphObjectList;
import com.facebook.model.GraphPlace;
import com.facebook.model.GraphUser;
import com.facebook.model.OpenGraphAction;

class CustomOpenGraphAction implements OpenGraphAction {
	public static final String PROPERTY_EXPLICITLY_SHARED = "explicitly_shared";
	public static final String PROPERTY_START_TIME = "start_time";
	public static final String PROPERTY_END_TIME = "end_time";
	
	private static final DateFormat ISO8601 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH);
	
	private final OpenGraphAction subAction;
	
	public CustomOpenGraphAction() {
		subAction = GraphObject.Factory.create(OpenGraphAction.class);
	}
	
	public String getId() {
		return subAction.getId();
	}
	
	public void setId(String id) {
		subAction.setId(id);
	}
	
	public String getType() {
		return subAction.getType();
	}
	
	public void setType(String type) {
		subAction.setType(type);
	}
	
	public Date getStartTime() {
		return subAction.getStartTime();
	}
	
	// cannot call subAction.setStartTime(Date) because on some locales it uses incorrect date formatting!
	public void setStartTime(Date startTime) {
		subAction.setProperty(PROPERTY_START_TIME, ISO8601.format(startTime));
	}
	
	public Date getEndTime() {
		return subAction.getEndTime();
	}
	
	// cannot call subAction.setEndTime(Date) because on some locales it uses incorrect date formatting!
	public void setEndTime(Date endTime) {
		subAction.setProperty(PROPERTY_END_TIME, ISO8601.format(endTime));
	}
	
	public <T extends GraphObject> T cast(Class<T> graphObjectClass) {
		return subAction.cast(graphObjectClass);
	}
	
	public Date getPublishTime() {
		return subAction.getPublishTime();
	}
	
	public void setPublishTime(Date publishTime) {
		subAction.setPublishTime(publishTime);
	}
	
	public Map<String, Object> asMap() {
		return subAction.asMap();
	}
	
	public Date getCreatedTime() {
		return subAction.getCreatedTime();
	}
	
	public void setCreatedTime(Date createdTime) {
		subAction.setCreatedTime(createdTime);
	}
	
	public JSONObject getInnerJSONObject() {
		return subAction.getInnerJSONObject();
	}
	
	public Date getExpiresTime() {
		return subAction.getExpiresTime();
	}
	
	public Object getProperty(String propertyName) {
		return subAction.getProperty(propertyName);
	}
	
	public void setExpiresTime(Date expiresTime) {
		subAction.setExpiresTime(expiresTime);
	}
	
	public String getRef() {
		return subAction.getRef();
	}
	
	public <T extends GraphObject> T getPropertyAs(String propertyName, Class<T> graphObjectClass) {
		return subAction.getPropertyAs(propertyName, graphObjectClass);
	}
	
	public void setRef(String ref) {
		subAction.setRef(ref);
	}
	
	public String getMessage() {
		return subAction.getMessage();
	}
	
	public void setMessage(String message) {
		subAction.setMessage(message);
	}
	
	public <T extends GraphObject> GraphObjectList<T> getPropertyAsList(String propertyName, Class<T> graphObjectClass) {
		return subAction.getPropertyAsList(propertyName, graphObjectClass);
	}
	
	public GraphPlace getPlace() {
		return subAction.getPlace();
	}
	
	public void setPlace(GraphPlace place) {
		subAction.setPlace(place);
	}
	
	public GraphObjectList<GraphObject> getTags() {
		return subAction.getTags();
	}
	
	public void setTags(List<? extends GraphObject> tags) {
		subAction.setTags(tags);
	}
	
	public void setProperty(String propertyName, Object propertyValue) {
		subAction.setProperty(propertyName, propertyValue);
	}
	
	public List<JSONObject> getImage() {
		return subAction.getImage();
	}
	
	public void setImage(List<JSONObject> image) {
		subAction.setImage(image);
	}
	
	public void removeProperty(String propertyName) {
		subAction.removeProperty(propertyName);
	}
	
	public void setImageUrls(List<String> urls) {
		subAction.setImageUrls(urls);
	}
	
	public GraphUser getFrom() {
		return subAction.getFrom();
	}
	
	public void setFrom(GraphUser from) {
		subAction.setFrom(from);
	}
	
	public JSONObject getLikes() {
		return subAction.getLikes();
	}
	
	public void setLikes(JSONObject likes) {
		subAction.setLikes(likes);
	}
	
	public GraphObject getApplication() {
		return subAction.getApplication();
	}
	
	public void setApplication(GraphObject application) {
		subAction.setApplication(application);
	}
	
	public JSONObject getComments() {
		return subAction.getComments();
	}
	
	public void setComments(JSONObject comments) {
		subAction.setComments(comments);
	}
	
	public GraphObject getData() {
		return subAction.getData();
	}
	
	public void setData(GraphObject data) {
		subAction.setData(data);
	}
	
	public boolean getExplicitlyShared() {
		return subAction.getExplicitlyShared();
	}
	
	public void setExplicitlyShared(boolean explicitlyShared) {
		// subAction.setExplicitlyShared(boolean) doesn't work, the same with parameter "fb:explicitly_shared"!
		subAction.setProperty("explicitly_shared", explicitlyShared);
	}
}
