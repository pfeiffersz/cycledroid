/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.facebook;

import java.util.List;

import com.maral.cycledroid.model.Unit.Distance;
import com.maral.cycledroid.model.Unit.Pace;
import com.maral.cycledroid.model.Unit.Speed;

class OpenGraphObjectCourse extends CustomOpenGraphObject {
	public static final String TYPE = "fitness.course";
	public static final String PROPERTY_DESCRIPTION = "og:description";
	public static final String PROPERTY_TITLE = "og:title";
	public static final String PROPERTY_DISTANCE_VALUE = "fitness:distance:value";
	public static final String PROPERTY_DISTANCE_UNIT = "fitness:distance:units";
	public static final String PROPERTY_DURATION_VALUE = "fitness:duration:value";
	public static final String PROPERTY_DURATION_UNIT = "fitness:duration:units";
	public static final String PROPERTY_SPEED_VALUE = "fitness:speed:value";
	public static final String PROPERTY_SPEED_UNIT = "fitness:speed:units";
	public static final String PROPERTY_CALORIES = "fitness:calories";
	public static final String PROPERTY_PACE_VALUE = "fitness:pace:value";
	public static final String PROPERTY_PACE_UNIT = "fitness:pace:units";
	public static final String PROPERTY_METRICS = "fitness:metrics";
	
	public OpenGraphObjectCourse() {
		super(TYPE);
	}
	
	@Override
	public void setTitle(String title) { // method in superclass causes an exception!
		setProperty(PROPERTY_TITLE, title);
	}
	
	@Override
	public void setDescription(String description) { // method in superclass causes an exception!
		setProperty(PROPERTY_DESCRIPTION, description);
	}
	
	public void setDistance(float distance, Distance unit) { // supports only km and mi
		setProperty(PROPERTY_DISTANCE_VALUE, distance);
		String unitText = null;
		switch(unit) {
			case KM:
				unitText = "km";
				break;
			case MI:
				unitText = "mi";
				break;
			case FT:
			case M:
			case NMI:
				throw new IllegalArgumentException(
						"Only miles and kilometers are allowed as distance units on Facebook.");
		}
		setProperty(PROPERTY_DISTANCE_UNIT, unitText);
	}
	
	public void setDurationInMs(float duration) {
		setProperty(PROPERTY_DURATION_VALUE, duration);
		setProperty(PROPERTY_DURATION_UNIT, "ms");
	}
	
	public void setSpeed(float speed, Speed unit) { // supports only m/s and ft/s
		setProperty(PROPERTY_SPEED_VALUE, speed);
		String unitText = null;
		switch(unit) {
			case FTS:
				unitText = "ft/s";
				break;
			case MS:
				unitText = "m/s";
				break;
			case KMH:
			case KN:
			case MPH:
				throw new IllegalArgumentException(
						"Only meters per second and feet per second are allowed as speed units on Facebook.");
		}
		setProperty(PROPERTY_SPEED_UNIT, unitText);
	}
	
	public void setCalories(float calories) { // kilocalories
		setProperty(PROPERTY_CALORIES, Math.round(calories));
	}
	
	public void setPace(float pace, Pace unit) { // supports only ms/m and ms/ft
		String unitText = null;
		switch(unit) {
			case MS_PER_FT:
				unitText = "s/ft";
				break;
			case MS_PER_M:
				unitText = "s/m";
				break;
			case S_PER_KM:
			case S_PER_MI:
			case S_PER_NMI:
				throw new IllegalArgumentException(
						"Only milliseconds per meter and milliseconds per foot are allowed as pace units on Facebook.");
		}
		setProperty(PROPERTY_PACE_VALUE, pace / 1000.0f); // convert to second/[unit]
		setProperty(PROPERTY_PACE_UNIT, unitText);
	}
	
	public void setMetrics(List<OpenGraphObjectMetric> metrics) {
		setProperty(PROPERTY_METRICS, metrics);
	}
}
