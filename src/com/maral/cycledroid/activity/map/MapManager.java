/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.map;

import java.util.List;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.maral.cycledroid.LocationBuilder;

class MapManager {
	private final PolylineOptions options;
	private final LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
	private Polyline polyline = null;
	private LatLng firstPoint = null;
	private LatLng lastPoint = null;
	private float bearing = Float.NaN;
	
	public MapManager(PolylineOptions options) {
		this.options = options;
	}
	
	public void attachPolyline(Polyline polyline) {
		this.polyline = polyline;
	}
	
	public void addLatLng(LatLng point) {
		if(firstPoint == null)
			firstPoint = point;
		if(lastPoint != null)
			bearing = toLocation(lastPoint).bearingTo(toLocation(point));
		lastPoint = point;
		options.add(point);
		if(polyline != null) {
			List<LatLng> points = polyline.getPoints();
			points.add(point);
			polyline.setPoints(points);
		}
		boundsBuilder.include(point);
	}
	
	public static Location toLocation(LatLng latLng) {
		return LocationBuilder.createLocation((float)latLng.latitude, (float)latLng.longitude);
	}
	
	public static LatLng toLatLng(Location location) {
		return new LatLng(location.getLatitude(), location.getLongitude());
	}
	
	public void addLocation(Location point) {
		addLatLng(toLatLng(point));
	}
	
	public LatLng getFirstPoint() {
		return firstPoint;
	}
	
	public LatLng getLastPoint() {
		return lastPoint;
	}
	
	public PolylineOptions getOptions() {
		return options;
	}
	
	public boolean isEmpty() {
		return firstPoint == null;
	}
	
	public LatLngBounds getBounds() {
		try {
			return boundsBuilder.build();
		} catch(IllegalStateException exception) {
			return null;
		}
	}
	
	public float getBearing() {
		return bearing;
	}
}
