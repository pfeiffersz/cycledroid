/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.ui;

import android.app.Activity;

public abstract class ActivityManager {
	private Activity activity;
	
	public ActivityManager(Activity activity) {
		this.activity = activity;
	}
	
	public void attachActivity(Activity activity) {
		this.activity = activity;
		performAction(activity);
	}
	
	public void detachActivity(Activity activity) {
		if(this.activity != activity)
			throw new IllegalArgumentException("Provided activity is not attached.");
		this.activity = null;
	}
	
	protected void performAction() {
		if(activity != null)
			performAction(activity);
	}
	
	protected abstract void performAction(Activity activity);
}
