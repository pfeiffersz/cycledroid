/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.exporter;

import java.io.File;
import java.io.Reader;
import java.io.Writer;
import java.util.Arrays;
import java.util.zip.DataFormatException;

import android.location.Location;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.maral.cycledroid.LocationBuilder;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.MyCursor;
import com.maral.cycledroid.model.Trip;

public class CSVExporter implements Exporter {
	private static final String[] CSV_HEADER_6 = new String[] {
		"latitude [degree]", "longitude [degree]", "altitude [meter]", "speed [meter per second]",
		"time (POSIX) [milisecond]", " part"
	}; // don't change, since files created with older versions won't be recognized
	private static final String[] CSV_HEADER_7 = new String[] {
		"latitude [degree]", "longitude [degree]", "altitude [meter]", "speed [meter per second]",
		"time (POSIX) [millisecond]", "total time [millisecond]", "name", "description"
	};
	
	private static final int INDEX_LATITUDE = 0;
	private static final int INDEX_LONGITUDE = 1;
	private static final int INDEX_ALTITUDE = 2;
	private static final int INDEX_SPEED = 3;
	private static final int INDEX_TIME = 4;
	private static final int INDEX_TOTAL_TIME = 5;
	private static final int INDEX_NAME = 6;
	private static final int INDEX_DESCRIPTION = 7;
	private static final int LINE_SIZE = 8;
	
	private final Database database;
	
	public CSVExporter(Database database) {
		this.database = database;
	}
	
	public boolean exportTrip(Trip trip, Writer writer, File file, ExportTripTask task) {
		task.updateProgress(0);
		boolean successful = false;
		CSVWriter csvWriter = null;
		database.beginTransaction();
		MyCursor pointsCursor = new MyCursor(database.getPointsCursor(trip, 1));
		try {
			int onePercent = pointsCursor.getCount() / 100;
			if(onePercent == 0) // to avoid dividing by zero in a loop
				onePercent = 1;
			int exported = 0;
			
			csvWriter = new CSVWriter(writer);
			csvWriter.writeNext(CSV_HEADER_7);
			
			String[] line = new String[LINE_SIZE];
			line[INDEX_TOTAL_TIME] = Long.toString((long)trip.getTotalTime());
			line[INDEX_NAME] = trip.getName();
			line[INDEX_DESCRIPTION] = trip.getDescription();
			csvWriter.writeNext(line);
			line[INDEX_TOTAL_TIME] = line[INDEX_NAME] = line[INDEX_DESCRIPTION] = "";
			for(pointsCursor.moveToFirst(); !pointsCursor.isAfterLast(); pointsCursor.moveToNext()) {
				line[INDEX_LATITUDE] = Float.toString(pointsCursor.getFloat(Database.POINT_LATITUDE));
				line[INDEX_LONGITUDE] = Float.toString(pointsCursor.getFloat(Database.POINT_LONGITUDE));
				line[INDEX_ALTITUDE] = Float.toString(pointsCursor.getFloat(Database.POINT_ALTITUDE));
				line[INDEX_SPEED] = Float.toString(pointsCursor.getFloat(Database.POINT_SPEED));
				line[INDEX_TIME] = Long.toString(pointsCursor.getLong(Database.POINT_TIME));
				csvWriter.writeNext(line);
				
				// don't do it often - it really slows:
				if(++exported % onePercent == 0)
					task.updateProgress((int)(exported / onePercent));
			}
			successful = true;
		} catch(Exception exception) {
			return false;
		} finally {
			task.updateProgress(100);
			pointsCursor.close();
			database.endTransaction(successful);
			try {
				if(csvWriter != null)
					csvWriter.close();
			} catch(Exception exception) {}
		}
		
		return true;
	}
	
	public Trip importTrip(Reader reader, File file, int lines, ImportTripTask task) {
		task.updateProgress(0);
		
		int onePercent = lines / 100;
		if(onePercent == 0) // avoid dividing by zero in a loop
			onePercent = 1;
		
		Trip trip = null;
		CSVReader csvReader = null;
		boolean successful = false;
		database.beginTransaction(); // much faster
		try {
			csvReader = new CSVReader(reader);
			String[] header = csvReader.readNext();
			int version = 0;
			if(Arrays.equals(header, CSV_HEADER_7))
				version = 7;
			else if(Arrays.equals(header, CSV_HEADER_6))
				version = 6;
			else
				throw new DataFormatException("No appropriate file header.");
			
			String[] line = null;
			long totalTime = 0;
			switch(version) {
				case 6:
					String[] parts = file.getAbsolutePath().split("/");
					int lastDot = parts[parts.length - 1].lastIndexOf(".");
					String tripName;
					if(lastDot == -1)
						tripName = parts[parts.length - 1];
					else
						tripName = parts[parts.length - 1].substring(0, lastDot);
					trip = database.createTripNoAdd(tripName, "");
					break;
				case 7:
					line = csvReader.readNext();
					totalTime = Long.parseLong(line[INDEX_TOTAL_TIME]);
					trip = database.createTripNoAdd(line[INDEX_NAME], line[INDEX_DESCRIPTION]);
					break;
				default:
					throw new RuntimeException("Invalid version.");
			}
			
			int imported = 1;
			while((line = csvReader.readNext()) != null) {
				float latitude = Float.parseFloat(line[INDEX_LATITUDE]);
				float longitude = Float.parseFloat(line[INDEX_LONGITUDE]);
				float altitude = Float.parseFloat(line[INDEX_ALTITUDE]);
				float speed = Float.parseFloat(line[INDEX_SPEED]);
				long time = Long.parseLong(line[INDEX_TIME]);
				Location point = LocationBuilder.createLocation(latitude, longitude, altitude, speed, time);
				database.addPoint(point, trip);
				
				// don't do it often - it really slows:
				if(++imported % onePercent == 0)
					task.updateProgress((int)(imported / onePercent));
			}
			
			trip.stop(); // clear values
			if(version == 7)
				database.increaseTotalTime(trip, totalTime - trip.getTotalTime());
			successful = true;
		} catch(Exception exception) {
			return null;
		} finally {
			task.updateProgress(100);
			database.endTransaction(successful);
			if(successful)
				database.addTrip(trip);
			try {
				if(csvReader != null)
					csvReader.close();
			} catch(Exception exception) {}
		}
		
		return trip;
	}
}
