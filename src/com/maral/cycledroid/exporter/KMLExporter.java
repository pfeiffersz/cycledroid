/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.exporter;

import java.io.File;
import java.io.Reader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.MyCursor;
import com.maral.cycledroid.model.Trip;

public class KMLExporter implements Exporter {
	private final Database database;
	
	public KMLExporter(Database database) {
		this.database = database;
	}
	
	private static String escape(String input) {
		input = input.replace("&", "&amp;");
		input = input.replace(">", "&gt;");
		input = input.replace("<", "&lt;");
		input = input.replace("'", "&apos;");
		input = input.replace("\"", "&quot;");
		input = input.replace("\r\n", "&#13;");
		return input;
	}
	
	@SuppressLint("SimpleDateFormat")
	public boolean exportTrip(Trip trip, Writer writer, File file, ExportTripTask task) {
		task.updateProgress(0);
		boolean successful = false;
		database.beginTransaction();
		MyCursor pointsCursor = new MyCursor(database.getPointsCursor(trip, 1));
		try {
			SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			
			int onePercent = pointsCursor.getCount() / 100;
			if(onePercent == 0) // to avoid dividing by zero in a loop
				onePercent = 1;
			int exported = 0;
			
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
			writer.write("<kml xmlns=\"http://www.opengis.net/kml/2.2\" "
					+ "xmlns:gx=\"http://www.google.com/kml/ext/2.2\" "
					+ "xmlns:kml=\"http://www.opengis.net/kml/2.2\" "
					+ "xmlns:atom=\"http://www.w3.org/2005/Atom\">\r\n");
			writer.write("<Document>\r\n");
			writer.write("\t<name>" + escape(file.getName()) + "</name>\r\n");
			writer.write("\t<Style id=\"sn_noicon\">\r\n");
			writer.write("\t\t<IconStyle><Icon></Icon></IconStyle>\r\n");
			writer.write("\t</Style>\r\n");
			
			writer.write("\t<Placemark>\r\n");
			writer.write("\t\t<name>" + escape(trip.getName()) + "</name>\r\n");
			writer.write("\t\t<styleUrl>#sn_noicon</styleUrl>\r\n");
			writer.write("\t\t<gx:Track>\r\n");
			for(pointsCursor.moveToFirst(); !pointsCursor.isAfterLast(); pointsCursor.moveToNext()) {
				Date time = new Date(pointsCursor.getLong(Database.POINT_TIME));
				writer.write("\t\t\t<when>" + timeFormatter.format(time) + "</when>\r\n");
				String longitude = Float.toString(pointsCursor.getFloat(Database.POINT_LONGITUDE));
				String latitude = Float.toString(pointsCursor.getFloat(Database.POINT_LATITUDE));
				String altitude = Float.toString(pointsCursor.getFloat(Database.POINT_ALTITUDE));
				writer.write("\t\t\t<gx:coord>" + longitude + " " + latitude + " " + altitude + "</gx:coord>\r\n");
				
				// don't do it often - it really slows:
				if(++exported % onePercent == 0)
					task.updateProgress((int)(exported / onePercent));
			}
			writer.write("\t\t</gx:Track>\r\n");
			writer.write("\t</Placemark>\r\n");
			writer.write("</Document>\r\n");
			writer.write("</kml>\r\n");
			successful = true;
		} catch(Exception exception) {
			return false;
		} finally {
			task.updateProgress(100);
			pointsCursor.close();
			database.endTransaction(successful);
		}
		
		return true;
	}
	
	public Trip importTrip(Reader reader, File file, int lines, ImportTripTask task) {
		throw new UnsupportedOperationException("KML file cannot be imported.");
	}
}
